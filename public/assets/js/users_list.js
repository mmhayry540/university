// var
let UserID = document.querySelector("#user_id"); // when edit User
let UserFullName = document.querySelector("#user_fullname");
let UserName = document.querySelector("#user_name");
let UserPassword = document.querySelector("#user_password");

let PerAddPenalty = document.querySelector("#add_penalty");
let PerCutPayment = document.querySelector("#cut_payment");
let PerConfirmPayment = document.querySelector("#confirm_payment");
let PerUsersPermessions = document.querySelector("#users_permessions");
let PerEmportExcel = document.querySelector("#emport_from_excel");

divPass = document.querySelector('#update-password');
divUser = document.querySelector('#update-username');

let UserActive = document.querySelector('#user_active_yes');
let UserNotActive = document.querySelector('#user_active_no');

let ModalID = document.querySelector("#add_edit_user");
let ModalBTN = document.querySelector('#ModalBTN');

// table
const table = document.querySelector("#users-table");

// form
const form = document.querySelector("#new-user-form");

// buttons
var SubmitBTN = document.querySelector("#save");
var loader = document.querySelector(".loader");
// ==============================================================

// Disable button and show spinner
function disableButton() {
  SubmitBTN.disabled = true; // Disable the button
  SubmitBTN.innerHTML =
    "<span class='fa fa-spinner fa-spin'></span> يرجى الإنتظار..."; // Change button content
}

// Enable button and reset content
function enableButton() {
  SubmitBTN.disabled = false; // Enable the button
  SubmitBTN.innerHTML = "حفظ العملية"; // Reset button content to original text
}

// get penalty List
function UsersList(page = 1) {
  loader.style.display = "flex";
  $("#users-table").html("");
  $.ajax({
    method: "GET",
    url: "getUsers",
    data: {
      page: page,
    },
    success: function (response) {
      setTimeout(() => {
        loader.style.display = "none"; // to hide loading

        $.each(response.users.users, function (key, user) {
          $("#users-table").append(`
                    <tr>
                         <td>${user['id']}</td>
                        <td>${user['name']}</td>
                        <td>${user['username']}</td>
                        <td>
                            ${(user['per_addpenalty'] == 1)?'<span class="badge bg-secondary">تغريم</span>': ''}
                            ${(user['per_cutpayment'] == 1)?'<span class="badge bg-secondary">قطع إيصال الدفع</span>': ''}
                            ${(user['per_confpayment'] == 1)?'<span class="badge bg-secondary">تثبيت إيصال الدقع</span>': ''}
                            ${(user['per_users'] == 1)?'<span class="badge bg-secondary">المستخدمين والسماحيات</span>': ''}
                            ${(user['per_import'] == 1)?'<span class="badge bg-secondary">إستيراد من الإكسل</span>': ''}
                        </td>
                        <td>
                            ${(user['active'] == 1)  ? '<span class="badge bg-success">فعال</span>' : '<span class="badge bg-danger">غير فعال</span>'} 
                        </td>
                        <td class="d-flex justify-content-around p-2">
                            <div class="mybutton btn-warning" id="edit_user" title="تعديل" data-id="${
                              user["id"]
                            }" onclick="EditUser(this)"><span class="fa fa-edit"></span></div>
                        </td>
                    </tr>
                `);
        });
      }, 200);
    },
    error: function (response) {
      console.log(response);
    },
  });
}

// Function to check required inputs
function CheckRequiredInputs(requiredInputFields) {
  let allFieldsFilled = true; // Flag to track if all required fields are filled

  requiredInputFields.forEach((input) => {
    if (input.value.trim() === "") {
      input.classList.add("error");
      allFieldsFilled = false;
    } else {
      input.classList.remove("error");
    }
  });

  return allFieldsFilled;
}

// Function to empty the form fields
function clearFormFields(form) {
  // Select and clear value of input[type=text] fields
  const textFields = form.querySelectorAll("input[type=text]");
  textFields.forEach((field) => {
    field.value = "";
  });

  // Select and set checked for input[type=radio] fields
  const radioFields = form.querySelectorAll("input[type=radio] , input[type=checkbox]");
  radioFields.forEach((field) => {
    field.checked = false;
  });

  UserActive.checked = true;
}


// Function to submit the form
function submitForm(form, url) {

  const formData = new FormData(form);

  $.ajax({
    url: url,
    type: "POST",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      enableButton();
      if (response.status == "success") {
        alertify.set("notifier", "position", "top-right");
        alertify.success(response.massage);
        clearFormFields(form);
        $('#add_edit_user').modal('hide');
        UsersList();
      } else {
        alertify.set("notifier", "position", "top-right");
        alertify.error(response.massage);
      }
    },
    error: function (error) {
      console.error("Error submitting form:", error);
    },
  });
}

// Function to edit penalty
function EditUser(button) {
  var UserId = $(button).data("id");
  $.ajax({
    method: "GET",
    url: "get_user_data_by_id",
    data: {
      id: UserId
    },
    success: function (response) {
      clearFormFields(form);

      UserID.value = response.user["id"];
      UserFullName.value = response.user["name"];
      UserName.value = response.user["username"];
      // Check the radio button based on the user active status
      if (response.user['active'] == 1) {
        UserActive.checked = true;
      } else {
        UserNotActive.checked = true;
      }

      if (response.user['per_addpenalty'] ==1) {
        PerAddPenalty.checked = true;
      }
      if (response.user['per_cutpayment'] ==1) {
        PerCutPayment.checked = true;
      }
      if (response.user['per_confpayment'] ==1) {
        PerConfirmPayment.checked = true;
      }
      if (response.user['per_users'] ==1) {
        PerUsersPermessions.checked = true;
      }
      if (response.user['per_import'] ==1) {
        PerEmportExcel.checked = true;
      }

   
      divUser.classList.remove('col-6');
      divUser.classList.add('col-12');
     divPass.style.display = 'none';

      // Show the modal by adding the 'show' class
      $('#add_edit_user').modal('show');
    },
    error: function (response) {
      console.log(response);
    },
  });
}


// Function to Extract Query Parameter
function getParameterByName(name, url) {
  // Create a URL object
  var urlObj = new URL(url);
  return urlObj.searchParams.get(name); // Get the value of the specified query parameter
}

// the Page is Ready
$(document).ready(function () {
  // Penalty list
  UsersList();

  ModalBTN.addEventListener("click" , function (){
    clearFormFields(form);
    divUser.classList.remove('col-12');
    divUser.classList.add('col-6');
   divPass.style.display = 'block';
  });

  // add n00000000ew penalty form
  form.addEventListener("submit", (event) => {
    event.preventDefault();

    if (CheckRequiredInputs([UserFullName, UserName, UserPassword])) {
      disableButton();
      submitForm(form, "add-edit-user");
    } else {
      return false;
    }
  });

  // pagination list
  $(".pagination li a").on("click", function (event) {
    var page = this.innerHTML;

    var url = $(this).attr("href"); // Get the URL from the clicked link
    var page = getParameterByName("page", url); // Extract the page number using a helper function
    event.preventDefault();

    $(".pagination li").removeClass("active");
    $(".pagination li a").removeClass("active");
    $(this).addClass("active");

    UsersList(page);
  });


});
