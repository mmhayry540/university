// var
let SuiteID = document.querySelector("#suite_id"); // when edit User
let UnitID = document.querySelector("#unit_id");
let SuiteName = document.querySelector("#suite_name");
let SuiteFirstRoom = document.querySelector("#suite_first_room");
let SuiteLastRoom = document.querySelector("#suite_last_room");

// modal ID
let ModalID = document.querySelector("#add_edit_suite");
let ModalBTN = document.querySelector('#ModalBTN');

// table
const table = document.querySelector("#suite-table");

// form
const form = document.querySelector("#new-suite-form");

// buttons
var SubmitBTN = document.querySelector("#save");
var loader = document.querySelector(".loader");
// ==============================================================

// Disable button and show spinner
function disableButton() {
  SubmitBTN.disabled = true; // Disable the button
  SubmitBTN.innerHTML =
    "<span class='fa fa-spinner fa-spin'></span> يرجى الإنتظار..."; // Change button content
}

// Enable button and reset content
function enableButton() {
  SubmitBTN.disabled = false; // Enable the button
  SubmitBTN.innerHTML = "حفظ العملية"; // Reset button content to original text
}

// get Suite List
function SuitesList(page = 1) {
  loader.style.display = "flex";
  $("#suite-table").html("");
  $.ajax({
    method: "GET",
    url: "getSuite",
    data: {
      page: page,
    },
    success: function (response) {


      setTimeout(() => {  
        loader.style.display = "none"; // to hide loading

        $.each(response.suites.suites, function (key, suite) {
          const rowId = `suite-row-${key}`; // Unique ID for each row
        
          $("#suite-table").append(`
            <tr id="${rowId}">
              <td>${suite['id']}</td>
              <td>${suite['name']}</td>
              <td>${suite['firstroom']}</td>
              <td>${suite['lastroom']}</td>
              <td id="unit-name-${key}">Loading...</td> <!-- Placeholder for unit name -->
              <td class="d-flex justify-content-around p-2">
                <div class="mybutton btn-warning" id="edit_unit" title="تعديل" data-id="${suite["id"]}" onclick="EditSuite(this)">
                  <span class="fa fa-edit"></span>
                </div>
              </td>
            </tr>
          `);
        
          // Fetch the unit name asynchronously and update the corresponding cell
          getUnitName(suite["housebuilding_id"], function(name) {
            $(`#unit-name-${key}`).text(name); // Update the cell with the fetched name
          });
        });
        
      }, 200);
    },
    error: function (response) {
      console.log(response);
    },
  });
}

// Function to check required inputs
function CheckRequiredInputs(requiredInputFields) {
  let allFieldsFilled = true; // Flag to track if all required fields are filled

  requiredInputFields.forEach((input) => {
    if (input.value.trim() === "") {
      input.classList.add("error");
      allFieldsFilled = false;
    } else {
      input.classList.remove("error");
    }
  });

  return allFieldsFilled;
}

// Function to empty the form fields
function clearFormFields(form) {
  // Select and clear value of input[type=text] fields
  const textFields = form.querySelectorAll("input , select");
  textFields.forEach((field) => {
    field.value = "";
  });

}


// Function to submit the form
function submitForm(form, url) {

  const formData = new FormData(form);

  $.ajax({
    url: url,
    type: "POST",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      enableButton();
      if (response.status == "success") {
        alertify.set("notifier", "position", "top-right");
        alertify.success(response.massage);
        $('#add_edit_suite').modal('hide');
        clearFormFields(form);
        SuitesList();
      } else {
        alertify.set("notifier", "position", "top-right");
        alertify.error(response.massage);
      }
    },
    error: function (error) {
      console.error("Error submitting form:", error);
    },
  });
}

// Function to edit unit
function EditSuite(button) {
  var suiteID = $(button).data("id");
  $.ajax({
    method: "GET",
    url: "get_suite_data_by_id",
    data: {
      id: suiteID
    },
    success: function (response) {
      clearFormFields(form);
      SuiteID.value = response.suite["id"];
      SuiteName.value = response.suite["name"];
      SuiteFirstRoom.value = response.suite["firstroom"];
      SuiteLastRoom.value = response.suite["lastroom"];
      UnitID.value = response.suite["housebuilding_id"];

      // Show the modal by adding the 'show' class
      $('#add_edit_suite').modal('show');
    },
    error: function (response) {
      console.log(response);
    },
  });
}


// Function to Extract Query Parameter
function getParameterByName(name, url) {
  // Create a URL object
  var urlObj = new URL(url);
  return urlObj.searchParams.get(name); // Get the value of the specified query parameter
}

// the Page is Ready
$(document).ready(function () {
  // Suite list
  SuitesList();

  ModalBTN.addEventListener("click" , function (){
    clearFormFields(form);
  });

  // add new unit form
  form.addEventListener("submit", (event) => {
    event.preventDefault();
    if (CheckRequiredInputs([SuiteName, SuiteFirstRoom,SuiteLastRoom,UnitID])) {
      disableButton();
      submitForm(form, form.action);
    } else {
      return false;
    }
  });

  // pagination list
  $(".pagination li a").on("click", function (event) {
    var page = this.innerHTML;

    var url = $(this).attr("href"); // Get the URL from the clicked link
    var page = getParameterByName("page", url); // Extract the page number using a helper function
    event.preventDefault();

    $(".pagination li").removeClass("active");
    $(".pagination li a").removeClass("active");
    $(this).addClass("active");

    SuitesList(page);
  });


});
