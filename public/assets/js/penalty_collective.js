// var
PenaltyID = document.querySelector("#penalty_id"); // when edit penalty
let UnitNumber = document.querySelector("#unit_number");
let SuitNumber = document.querySelector("#suit_number");
let PenaltyValue = document.querySelector("#penalty_value");
let PenaltyReason = document.querySelector("#penalty_reason");
let PenaltyDetails = document.querySelector("#panalty_details");
let supervisorBookDate = document.querySelector("#supervisor_book_date");
let supervisorBookNumber = document.querySelector("#supervisor_book_number");
let OrderId = document.querySelector("#order_id");
let OrderDate = document.querySelector("#order_date");
let TextSearch = document.querySelector('#text_search');


// table
const table = document.querySelector("#collective-penalty-table");

// form
const form = document.querySelector("#collective-penalty-form");

// buttons
var SubmitBTN = document.querySelector("#save");
var NewBTN = document.querySelector("#new");
var loader = document.querySelector(".loader");
var SearchBTN = document.querySelector('#search_btn');

// ==============================================================

// Disable button and show spinner
function disableButton() {
  SubmitBTN.disabled = true; // Disable the button
  SubmitBTN.innerHTML =
    "<span class='fa fa-spinner fa-spin'></span> يرجى الإنتظار..."; // Change button content
}

// Enable button and reset content
function enableButton() {
  SubmitBTN.disabled = false; // Enable the button
  SubmitBTN.innerHTML = "حفظ العملية"; // Reset button content to original text
}

// get penalty List
function PenaltyList(search = '') {
  loader.style.display = "flex";
  $("#collective-penalty-table").html("");
  $.ajax({
    method: "GET",
    url: "penalty_list",
    data: {
      'type': "collective",
      'search': search,
    },
    success: function (response) {
      setTimeout(() => {
        loader.style.display = "none"; // to hide loading

        $.each(response.penalty, function (key, penalty) {
          const rowId = `collective-penalty-row-${key}`; // Unique ID for each row
        
          $("#collective-penalty-table").append(`
            <tr id="${rowId}">
              <td>${penalty["id"]}</td>
              <td id="unit-name-${key}">Loading...</td> <!-- Placeholder for unit name -->
              <td>${penalty["suiteno"]}</td>
              <td>${penalty["value"]}<span class='small text-secondary px-1'>ل.س</span></td>
              <td>${penalty["supervisor_orderdate"]}<span class='small text-secondary px-1'>م</span></td>
              <td>${penalty["cause"]}</td>
              <td class="d-flex justify-content-around p-2">
                <div class="mybutton btn-info" title="تعديل" id="edit_penalty" data-id="${penalty["id"]}" onclick="EditPenalty(this)">
                  <span class="fa fa-edit"></span>
                </div>
                <a target="_blank" title="طباعة" href="print_collective_penalty/${penalty["id"]}" class="mybutton btn-warning">
                  <span class="fa fa-print"></span>
                </a>
              </td>
            </tr>
          `);
        
          // Fetch the unit name asynchronously and update the corresponding cell
          getUnitName(penalty["housebuilding_id"], function(name) {
            $(`#unit-name-${key}`).text(name); // Update the cell with the fetched name
          });
        });
        
      }, 200);
    },
    error: function (response) {
      console.log(response);
    },
  });
}

// Function to check required inputs
function CheckRequiredInputs(requiredInputFields) {
  let allFieldsFilled = true; // Flag to track if all required fields are filled

  requiredInputFields.forEach((input) => {
    if (input.value.trim() === "") {
      input.classList.add("error");
      allFieldsFilled = false;
    } else {
      input.classList.remove("error");
    }
  });

  return allFieldsFilled;
}

// Function to empty the form fields
function clearFormFields(form) {
  const formFields = form.querySelectorAll("input, textarea, select");
  formFields.forEach((field) => {
    field.value = "";
  });

}

// Function to submit the form
function submitForm(form, url) {
  // Convert array to JSON string

  const formData = new FormData(form);

  $.ajax({
    url: url,
    type: "POST",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      enableButton();
      if (response.status == "success") {
        alertify.set("notifier", "position", "top-right");
        alertify.success(response.massage);
        clearFormFields(form);
        PenaltyList();
      } else {
        alertify.set("notifier", "position", "top-right");
        alertify.error(response.massage);
      }
    },
    error: function (error) {
      console.error("Error submitting form:", error);
    },
  });
}

// Function to edit penalty
function EditPenalty(button) {
  var penaltyId = $(button).data("id");
  $.ajax({
    method: "GET",
    url: "get_penalty_data_by_id",
    data: {
      id: penaltyId,
      type: "collective",
    },
    success: function (response) {
      clearFormFields(form);
      document.documentElement.scrollTop = 0;
      PenaltyID.value = response.penalty["id"];
      UnitNumber.value = response.penalty["housebuilding_id"];
      SuitNumber.value = response.penalty["suiteno"];
      PenaltyValue.value = response.penalty["value"];
      supervisorBookDate.value = response.penalty["supervisor_orderdate"];
      supervisorBookNumber.value = response.penalty["supervisor_orderid"];
      OrderId.value = response.penalty["orderid"];
      OrderDate.value = response.penalty["orderdate"];
      PenaltyReason.value = response.penalty["cause"];
      PenaltyDetails.value = response.penalty["details"];
    },
    error: function (response) {
      console.log(response);
    },
  });
}



// the Page is Ready
$(document).ready(function () {
  // Penalty list
  PenaltyList();
  // add new penalty form
  form.addEventListener("submit", (event) => {
    event.preventDefault();

    if (CheckRequiredInputs([SuitNumber, PenaltyValue, UnitNumber])) {
      disableButton();
      submitForm(form, "penalty-collective");
    } else {
      return false;
    }
  });

  UnitNumber.addEventListener('change',  function(){
    let unit = this.value;
    $.ajax({
      method: "GET",
      url: "get_suites_by_unit_id",
      data: {
        'unit': unit,
      },
      success: function (response) {
        SuitNumber.innerHTML = "";
        $("#suit_number").append(`<option  value="">${"اختر الجناح"}</option>`);
        if(response.suites.length === 0)
        {
        $("#suit_number").append(`<option class="text-danger" disabled>${"لا يوجد بيانات"}</option>`);
        }
        $.each(response.suites, function (key, st) {
          var option = document.createElement("option");
          option.value = st["id"];
          option.text = st["name"];

          SuitNumber.append(option);
        });
      },
      error: function (response) {
        console.log(response);
      },
    });
    


  });

  // reset form action
  NewBTN.addEventListener("click", function () {
    clearFormFields(form);
    UnitNumber.classList.remove("error");
    SuitNumber.classList.remove("error");
    PenaltyValue.classList.remove("error");
    PenaltyID.value = '';
    form.action = "penalty-collective";
  });

  SearchBTN.addEventListener("click" , function(){
    search = TextSearch.value;
    PenaltyList(search);
  });
});
