// var
let UnitID = document.querySelector("#unit_id"); // when edit User
let UnitName = document.querySelector("#unit_name");
let Supervisor = document.querySelector('#supervisor_name');
let UnitType = document.querySelector("#unit_type");

// modal ID
let ModalID = document.querySelector("#add_edit_unit");
let ModalBTN = document.querySelector('#ModalBTN');

// table
const table = document.querySelector("#units-table");

// form
const form = document.querySelector("#new-unit-form");

// buttons
var SubmitBTN = document.querySelector("#save");
var loader = document.querySelector(".loader");
// ==============================================================

// Disable button and show spinner
function disableButton() {
  SubmitBTN.disabled = true; // Disable the button
  SubmitBTN.innerHTML =
    "<span class='fa fa-spinner fa-spin'></span> يرجى الإنتظار..."; // Change button content
}

// Enable button and reset content
function enableButton() {
  SubmitBTN.disabled = false; // Enable the button
  SubmitBTN.innerHTML = "حفظ العملية"; // Reset button content to original text
}

// get Unis List
function UnitsList(page = 1) {
  loader.style.display = "flex";
  $("#units-table").html("");
  $.ajax({
    method: "GET",
    url: "getUnits",
    data: {
      page: page,
    },
    success: function (response) {

      setTimeout(() => {  
        loader.style.display = "none"; // to hide loading

        $.each(response.units.units, function (key, un) {
          $("#units-table").append(`
                    <tr>
                         <td>${un['id']}</td>
                        <td>${un['name']}</td>
                        <td>${un['supervisor']}</td>
                        <td>${getUnitType(un['type'])}</td>
                      
                        <td class="d-flex justify-content-around p-2">
                            <div class="mybutton btn-warning" title="تعديل" id="edit_unit" data-id="${
                              un["id"]
                            }" onclick="EditUnit(this)"><span class="fa fa-edit"></span></div>
                        </td>
                    </tr>
                `);
        });
      }, 200);
    },
    error: function (response) {
      console.log(response);
    },
  });
}

// Function to check required inputs
function CheckRequiredInputs(requiredInputFields) {
  let allFieldsFilled = true; // Flag to track if all required fields are filled

  requiredInputFields.forEach((input) => {
    if (input.value.trim() === "") {
      input.classList.add("error");
      allFieldsFilled = false;
    } else {
      input.classList.remove("error");
    }
  });

  return allFieldsFilled;
}

// Function to empty the form fields
function clearFormFields(form) {
  // Select and clear value of input[type=text] fields
  const textFields = form.querySelectorAll("input , select");
  textFields.forEach((field) => {
    field.value = "";
  });

}


// Function to submit the form
function submitForm(form, url) {

  const formData = new FormData(form);

  $.ajax({
    url: url,
    type: "POST",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      enableButton();
      if (response.status == "success") {
        alertify.set("notifier", "position", "top-right");
        alertify.success(response.massage);
        $('#add_edit_unit').modal('hide');
        clearFormFields(form);
        UnitsList();
      } else {
        alertify.set("notifier", "position", "top-right");
        alertify.error(response.massage);
      }
    },
    error: function (error) {
      console.error("Error submitting form:", error);
    },
  });
}

// Function to edit unit
function EditUnit(button) {
  var unitId = $(button).data("id");
  $.ajax({
    method: "GET",
    url: "get_unit_data_by_id",
    data: {
      id: unitId
    },
    success: function (response) {
      clearFormFields(form);

      UnitID.value = response.unit["id"];
      UnitName.value = response.unit["name"];
      Supervisor.value = response.unit["supervisor"];
      UnitType.value = response.unit["type"];

      // Show the modal by adding the 'show' class
      $('#add_edit_unit').modal('show');
    },
    error: function (response) {
      console.log(response);
    },
  });
}


// Function to Extract Query Parameter
function getParameterByName(name, url) {
  // Create a URL object
  var urlObj = new URL(url);
  return urlObj.searchParams.get(name); // Get the value of the specified query parameter
}

// the Page is Ready
$(document).ready(function () {
  // units list
  UnitsList();

  ModalBTN.addEventListener("click" , function (){
    clearFormFields(form);
  });


  // add new unit form
  form.addEventListener("submit", (event) => {
    event.preventDefault();
    if (CheckRequiredInputs([UnitName, UnitType , Supervisor])) {
      disableButton();
      submitForm(form, form.action);
    } else {
      return false;
    }
  });

  // pagination list
  $(".pagination li a").on("click", function (event) {
    var page = this.innerHTML;

    var url = $(this).attr("href"); // Get the URL from the clicked link
    var page = getParameterByName("page", url); // Extract the page number using a helper function
    event.preventDefault();

    $(".pagination li").removeClass("active");
    $(".pagination li a").removeClass("active");
    $(this).addClass("active");

    UnitsList(page);
  });


});
