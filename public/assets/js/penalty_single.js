// var
let PenaltyID = document.querySelector("#penalty_id"); // when edit penalty
let UnitNumber = document.querySelector("#unit_number");
let RoomNumber = document.querySelector("#room_number");
let PenaltyValue = document.querySelector("#penalty_value");
let PenaltyReason = document.querySelector("#penalty_reason");
let PenaltyDetails = document.querySelector("#panalty_details");
let supervisorBookDate = document.querySelector("#supervisor_book_date");
let supervisorBookNumber = document.querySelector("#supervisor_book_number");
let OrderId = document.querySelector("#order_id");
let OrderDate = document.querySelector("#order_date");
let StudentAria = document.querySelector(".student-aria");
let StudentSelect = document.querySelector("#students");
let TextSearch = document.querySelector('#text_search');
var StudentList = [];

// table
const table = document.querySelector("#single-penalty-table");

// form
const form = document.querySelector("#single-penalty-form");

// buttons
var SubmitBTN = document.querySelector("#save");
var NewBTN = document.querySelector("#new");
var loader = document.querySelector(".loader");
var SearchBTN = document.querySelector('#search_btn');
// ==============================================================

// Disable button and show spinner
function disableButton() {
  SubmitBTN.disabled = true; // Disable the button
  SubmitBTN.innerHTML =
    "<span class='fa fa-spinner fa-spin'></span> يرجى الإنتظار..."; // Change button content
}

// Enable button and reset content
function enableButton() {
  SubmitBTN.disabled = false; // Enable the button
  SubmitBTN.innerHTML = "حفظ العملية"; // Reset button content to original text
}

// get penalty List
function PenaltyList(search = '') {
  loader.style.display = "flex";
  $("#single-penalty-table").html("");
  $.ajax({
    method: "GET",
    url: "penalty_list",
    data: {
      'type': "single",
      'search': search,
    },
    success: function (response) {
      setTimeout(() => {
        loader.style.display = "none"; // to hide loading

        $.each(response.penalty, function (key, penalty) {
          const rowId = `penalty-row-${key}`; // Unique ID for each row
        
          $("#single-penalty-table").append(`
            <tr id="${rowId}">
              <td>${penalty["id"]}</td>
              <td id="unit-name-${key}">Loading...</td> <!-- Placeholder for unit name -->
              <td>${penalty["roomno"]}</td>
              <td>${penalty["value"]}<span class='small text-secondary px-1'>ل.س</span></td>
              <td>${penalty["supervisor_orderdate"]}<span class='small text-secondary px-1'>م</span></td>
              <td>${penalty["cause"]}</td>
              <td class="d-flex justify-content-around p-2">
                <div class="mybutton mx-2 btn-info" title="تعديل" id="edit_penalty" data-id="${penalty["id"]}" onclick="EditPenalty(this)">
                  <span class="fa fa-edit"></span>
                </div>
                <a target="_blank" href="print_single_penalty/${penalty["id"]}" title="طباعة" class="mybutton btn-warning">
                  <span class="fa fa-print"></span>
                </a>
              </td>
            </tr>
          `);
        
          // Fetch the unit name asynchronously and update the corresponding cell
          getUnitName(penalty["housebuilding_id"], function(name) {
            $(`#unit-name-${key}`).text(name); // Update the cell with the fetched name
          });
        });
        
      }, 200);
    },
    error: function (response) {
      console.log(response);
    },
  });
}

// Function to check required inputs
function CheckRequiredInputs(requiredInputFields) {
  let allFieldsFilled = true; // Flag to track if all required fields are filled

  requiredInputFields.forEach((input) => {
    if (input.value.trim() === "") {
      input.classList.add("error");
      allFieldsFilled = false;
    } else {
      input.classList.remove("error");
    }
  });

  return allFieldsFilled;
}

// Function to empty the form fields
function clearFormFields(form) {
  const formFields = form.querySelectorAll("input, textarea, select");
  formFields.forEach((field) => {
    field.value = "";
  });
  StudentList = [];
  AStudentList();
}

// Function to submit the form
function submitForm(form, url) {
  // Convert array to JSON string
  const jsonData = JSON.stringify(StudentList);

  const formData = new FormData(form);

  formData.append("students", jsonData);

  $.ajax({
    url: url,
    type: "POST",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      enableButton();
      if (response.status == "success") {
        alertify.set("notifier", "position", "top-right");
        alertify.success(response.massage);
        clearFormFields(form);
        PenaltyList();
      } else {
        alertify.set("notifier", "position", "top-right");
        alertify.error(response.massage);
      }
    },
    error: function (error) {
      console.error("Error submitting form:", error);
    },
  });
}

// Function to edit penalty
function EditPenalty(button) {
  var penaltyId = $(button).data("id");
  $.ajax({
    method: "GET",
    url: "get_penalty_data_by_id",
    data: {
      id: penaltyId,
      type: "single",
    },
    success: function (response) {
      clearFormFields(form);

      let student_penalty = response.penalty["students_penalty"];
      let student_room = response.penalty["student_room"];

      // Extract id and fullname from students_penalty and merge with StudentsList
      let extractedStudents = student_penalty.map((student) => ({
        id: student.id,
        name: student.fullname,
      }));
      StudentList = extractedStudents;
      AStudentList();

      StudentSelect.innerHTML = "";
      $("#students").append(`<option  value="">${"اختر الطلاب"}</option>`);
      $.each(student_room, function (key, st) {
        var option = document.createElement("option");
        option.value = st["id"];
        option.text = st["fullname"];

        StudentSelect.append(option);
      });

      document.documentElement.scrollTop = 0;
      form.action = "edit-penalty";
      PenaltyID.value = response.penalty["id"];
      UnitNumber.value = response.penalty["housebuilding_id"];
      RoomNumber.value = response.penalty["roomno"];
      PenaltyValue.value = response.penalty["value"];
      supervisorBookDate.value = response.penalty["supervisor_orderdate"];
      supervisorBookNumber.value = response.penalty["supervisor_orderid"];
      OrderId.value = response.penalty["orderid"];
      OrderDate.value = response.penalty["orderdate"];
      PenaltyReason.value = response.penalty["cause"];
      PenaltyDetails.value = response.penalty["details"];
    },
    error: function (response) {
      console.log(response);
    },
  });
}

// student list
function AStudentList() {
  StudentAria.innerHTML = "";
  $.each(StudentList, function (key, stu) {
    StudentAria.innerHTML += `
                    <div class="stud">
                        <div id="st_name">${stu["name"]}</div>
                        <div id="st_delete" title="حذف" class="st_delete" data-id="${stu["id"]}">
                            <span class="fa text-danger fa-times"></span>
                        </div>
                    </div>
                `;
  });

  // Add event listeners to delete buttons after rendering the list
  document.querySelectorAll(".st_delete").forEach(function (element) {
    element.addEventListener("click", function () {
      deleteStudentFromList(this.getAttribute("data-id"));
    });
  });
}

// delete student from list
function deleteStudentFromList(id) {
  StudentList = StudentList.filter((student) => student.id !== id);

  AStudentList(); // Refresh the list
}

function checkIdInArray(id, array) {
  return array.some(student => student.id === id);
}

// the Page is Ready
$(document).ready(function () {
  // Penalty list
  PenaltyList();
  // student list
  AStudentList();

  // add new penalty form
  form.addEventListener("submit", (event) => {
    event.preventDefault();

    if (CheckRequiredInputs([RoomNumber, PenaltyValue, UnitNumber])) {
      disableButton();
      submitForm(form, "penalty-single");
    } else {
      return false;
    }
  });

  // reset form action
  NewBTN.addEventListener("click", function () {
    clearFormFields(form);
    UnitNumber.classList.remove("error");
    RoomNumber.classList.remove("error");
    PenaltyValue.classList.remove("error");

    form.action = "penalty-single";
  });

  // clear Room number when change unit 
  UnitNumber.addEventListener('change' , function(){

    RoomNumber.value ='';
  })

  // get name student by room id
  RoomNumber.addEventListener("keyup", function () {
    var num = this.value;
    if (num.length <= 2 || num.length >= 4) {
      StudentSelect.innerHTML = "";
      $("#students").append(`<option  value="">${"اختر الطلاب"}</option>`);
      return false;
    } else {
      $.ajax({
        method: "get",
        url: "get_students_by_room_id",
        data: {
          num: num,
          unit: UnitNumber.value,
        },
        success: function (response) {
          StudentSelect.innerHTML = "";
          $("#students").append(`<option  value="">${"اختر الطلاب"}</option>`);
          $.each(response.student, function (key, st) {
            var option = document.createElement("option");
            option.value = st["id"];
            option.text = st["fullname"];

            StudentSelect.append(option);
          });
        },
      });
    }
  });

  // add student to list
  StudentSelect.addEventListener("change", (student) => {
    var selectedOption = StudentSelect.options[StudentSelect.selectedIndex];

    // Get the ID and name of the selected student
    var id = selectedOption.value;
    var name = selectedOption.text;
    
    
    if (id != "") {
      if (!checkIdInArray(id, StudentList)) {
        // Add the selected student to the StudentList array
        StudentList.push({
            id: id,
            name: name,
        });
        AStudentList();
      } else {
        alertify.set("notifier", "position", "top-right");
        alertify.error('اسم الطالب موجود مسبقاً ؟؟!!');
    }
      
    }
  });

  SearchBTN.addEventListener("click" , function(){
    search = TextSearch.value;
    PenaltyList(search);
  });
});
