<?php

namespace App\Models;

use CodeIgniter\Model;

class Suits_Model extends Model
{
    protected $table      = 'suite';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'name',
        'firstroom',
        'lastroom',
        'housebuilding_id'
    ];

    protected bool $allowEmptyInserts = false;
    protected bool $updateOnlyChanged = true;

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = false;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];




    // functions

    public function getSuitesList($page = 1)
    {
        $perPage = 10;

        $Suites = $this->paginate($perPage, 'default', $page);
        
        // Return both Suites and the pager object
        return [
            'suites' => $Suites,
            'pager' => $this->pager
        ];    }





}