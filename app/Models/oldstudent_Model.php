<?php

namespace App\Models;

use CodeIgniter\Model;

class oldstudent_Model extends Model
{
    protected $table      = 'studentsold';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'nationID',
        'fullname',
        'mother',
        'mobile',
        'address',
        'notes',
        'unit',
        'room',
        'university',
        'study_year',
        'study_season',
        'status'
    ];

    protected bool $allowEmptyInserts = false;
    protected bool $updateOnlyChanged = true;

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = false;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];




    // functions
    public function getNewStudentsList($page = 1)
    {
        $perPage = 10;

        $students = $this->paginate($perPage, 'default', $page);
        
        // Return both students and the pager object
        return [
            'students' => $students,
            'pager' => $this->pager
        ];    }

 



}