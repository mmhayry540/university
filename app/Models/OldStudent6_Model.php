<?php

namespace App\Models;

use CodeIgniter\Model;

class OldStudent6_Model extends Model
{
    protected $table      = 'oldstudents6';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'NationalID'  ,
        'study_year'  ,
         'study_season'  ,
         'fullname'  ,
         'father'  ,
         'mother'  ,
         'faculty'  ,
         'student_id'  ,
         'partname'  ,
         'year'  ,
         'city'  ,
         'birthdate_place' ,
         'kaid' ,
         'address' ,
         'building_id' ,
         'room_id' ,
         'penalty_detail' ,
         'penalty_date' ,
         'penalty_value' ,
         'order_id' ,
         'order_date' ,
         'order_notice1' ,
         'order_notice2' ,
    ];
/*
    protected bool $allowEmptyInserts = false;
    protected bool $updateOnlyChanged = true;

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = false;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];


*/

    // functions

 



}