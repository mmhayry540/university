<?php

namespace App\Models;

use CodeIgniter\Model;

class Penalty_Single_Model extends Model
{
    protected $table      = 'penaltysingals';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'housebuilding_id',
        'roomno',
        'value',
        'cause',
        'details',
        'supervisor_orderid',
        'supervisor_orderdate',
        'orderid',
        'orderdate'
    ];

    /*protected bool $allowEmptyInserts = false;
    protected bool $updateOnlyChanged = true;

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = false;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
*/



    // functions
    public function getSinglePenaltyList($search = '')
    {

        if ($search != '') {
            $penalties = $this->orderBy('id', 'desc')->where('id', $search)->findAll();
        } else {
            $penalties = $this->orderBy('id', 'desc')->findAll();
        }
        return ['penalty' => $penalties];
    }

    public function get_unit_room_penalty($pid)
    {

        $query = $this->db->query('select housebuilding_id, roomno from  penaltysingals where id  = ?', [$pid]);
        return $query->getRow();
    }
}
