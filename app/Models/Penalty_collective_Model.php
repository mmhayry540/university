<?php

namespace App\Models;

use CodeIgniter\Model;

class Penalty_collective_Model extends Model
{
    protected $table      = 'penaltycollectives';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'housebuilding_id',
        'suiteno',
        'value',
        'cause',
        'details',
        'supervisor_orderid',
        'supervisor_orderdate',
        'orderid',
        'orderdate'
    ];

    /*protected bool $allowEmptyInserts = false;
    protected bool $updateOnlyChanged = true;

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = false;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

*/


    // functions

    public function getCollectivePenaltyList($search = '')
    {

        if($search != '')
        {
            $penalties = $this->orderBy('id', 'desc')->where('id', $search)->findAll();
        }else{
            $penalties = $this->orderBy('id', 'desc')->findAll();
        }

        return ['penalty' => $penalties,];
    }

    public function deletestudent_fromcollectivepenalty($pid)
    {

        $query = $this->db->query('DELETE FROM `studentpenalty` where penaltyid = ? and ptype = 1', [$pid]);
        $affectedRows = $this->db->affectedRows();
        if ($affectedRows > 0 ) return true; else return false;
    } 
}
