<?php

namespace App\Models;

use CodeIgniter\Model;

class Housing_Unit_model extends Model
{
    protected $table      = 'house_unit';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'name',
        'supervisor',
        'type'
    ];

   /* protected bool $allowEmptyInserts = false;
    protected bool $updateOnlyChanged = true;

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = false;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];*/




    // functions

    public function getUnitsList($page = 1)
    {
        $perPage = 10;

        $Units = $this->paginate($perPage, 'default', $page);
        
        // Return both Units and the pager object
        return [
            'units' => $Units,
            'pager' => $this->pager
        ];   
     }

     public function GetNameUnitById($unit_id){
        $query = $this->db->query('select name from house_unit WHERE id = ?', [$unit_id]);
        return $query->getRow()->name;

     }



}