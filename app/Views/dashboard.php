<?= $this->extend('layouts/master_2'); ?>

<?= $this->section('content'); ?>

<!-- Search Box -->
<div class="box">
    <div class="row">
        <div class="col-md-8">
            <label for="text_search">البحث بالأسم الثلاثي أو الرقم الوطني أو رقم الجوال </label>
            <input type="text" name="text_search" id="text_search" class="form-control mt-1" placeholder="ادخل الاسم أو الرقم الوطني أو رقم الجوال">
        </div>
        <div class="col-md-3">
            <label for="status_search">الحالة</label>
            <select name="status_search" id="status_search" class="form-control mt-1">
                <option value="-1" selected>الكل</option>
                <option value="0">بريء الزمة</option>
                <option value="1">عليه غرامة</option>
            </select>
        </div>
        <div class="col-md-1">
            <button type="submit" id="search_btn" class="mybutton btn-warning mt-4">بحث</button>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="loader">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <div class="col-6">
        <!-- Table Box for New Data -->
        <table class="table table-bordered box">
            <caption class="caption-table caption-top shadow">بيانات الطلاب</caption>
            <thead class="bg-warning">
                <th>#</th>
                <th>الرقم الوطني</th>
                <th>الأسم الثلاثي</th>
                <th>الجوال</th>
                <th>الحالة</th>
                <th width="80">عمليات</th>
            </thead>
            <tbody id="New-Data-table">

            </tbody>
        </table>
    </div>
    <div class="col-6">
        <!-- Table Box for Old Data -->
        <table class="table table-bordered box">
            <caption class="caption-table caption-top shadow">البيانات القديمة</caption>
            <thead class="bg-primary">
                <th>#</th>
                <th>الأسم الثلاثي</th>
                <th colspan="3">تفاصيل</th>

                <th>عمليات</th>
            </thead>
            <tbody id="Old-Data-Table">

            </tbody>
        </table>

    </div>
</div>






<!-- Modal -->
<div class="modal fade" id="student_info" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h5 class="modal-title" id="staticBackdropLabel">تفاصيل الطالب</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-3 p-1">
                        <label for="fullname">الاسم الثلاثي :</label>
                    </div>
                    <div class="col-3 p-1">
                        <div class="text-primary fw-bold" id="fullname"></div>
                    </div>

                    <div class="col-3 p-1">
                        <label for="nationID">الرقم الوطني :</label>
                    </div>
                    <div class="col-3 p-1">
                        <div class="text-primary fw-bold" id="nationID"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 p-1">
                        <label for="">اسم الأم :</label>
                    </div>
                    <div class="col-3 p-1">
                        <div class="text-primary fw-bold" id="motherName"></div>
                    </div>

                    <div class="col-3 p-1">
                        <label for="">رقم الهاتف :</label>
                    </div>
                    <div class="col-3 p-1">
                        <div class="text-primary fw-bold" id="mobile"></div>
                    </div>

                </div>
                <br>
                <hr>
                <div class="d-flex">
                    <h5><u>الغرامات المترتبة على الطالب :</u></h5>
                    <span id="status" class="mx-2"></span>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <th>#</th>
                        <th>رقم الغرامة</th>
                        <th>قيمة الغرامة</th>
                        <th>نوع الغرامة</th>
                        <th>الوحدة</th>
                        <th>الغرفة/الجناح</th>
                        <th>الكلية</th>
                        <th>حالة الغرامة</th>
                        <th>عمليات</th>
                    </thead>
                    <tbody id="penalty_list">


                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="mybutton btn-secondary" data-bs-dismiss="modal">إغلاق</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade mt-md-2" id="confirm_penalty_modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <?= form_open('/add_payment_penalty') ?>
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h5 class="modal-title" id="staticBackdropLabel">تأكيد عملية الدفع</h5>
            </div>
            <div class="modal-body">
                <input type="hidden" name="penaltyid" id="penaltyid">
                <input type="hidden" name="studentid" id="studentid">
                <input type="hidden" name="penaltyType" id="penaltyType">
                <div class="row">
                    <div class="col-md-12">
                        <label for="confirm_penalty_num">رقم الإيصال :</label>
                        <input type="text" name="confirm_penalty_num" id="confirm_penalty_num" class="form-control mt-1" placeholder="ادخل رقم الإيصال">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label for="confirm_penalty_date">تاريخ الإيصال</label>
                        <input type="date" name="confirm_penalty_date" id="confirm_penalty_date" class="form-control mt-1" placeholder="ادخل تاريخ الإيصال">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="ok_btn" class="mybutton btn-warning">تسديد</button>

                <button type="button" class="mybutton btn-secondary" data-bs-dismiss="modal">إغلاق</button>
            </div>
        </div>
        <?= form_close() ?>
    </div>
</div>

<?= script_tag('public/assets/js/dashboard.js');  ?>

<?= $this->endSection(); ?>