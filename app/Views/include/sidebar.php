<aside id="aside-main" class="aside-start sidebar-open aside-hide-xs bg-white shadow-sm d-flex flex-column px-2 h-auto">

<button class="sidebar-btn">
<i class="fa fa-arrow-right"></i>
</button>

  <!-- sidebar : logo -->
  <div class="d-flex pt-3 px-3 mb-3 mt-3">
    <a href="<?= base_url() ?>/dashboard">
      <img class="rounded-circle" src="<?= base_url('public/assets/images/login_image.jpg') ?>" width="50" height="50" alt="...">
    </a>
    <div class="fs-5 mt-2 mx-2"><b>لوحة التحكم</b></div>
  </div>
  <!-- /sidebar : logo -->


  <!-- sidebar : navigation -->
  <div class="aside-wrapper scrollable-vertical scrollable-styled-light align-self-baseline h-100 w-100">

    <nav class="nav-deep nav-deep-sm nav-deep-light">
      <ul class="nav flex-column">

        <li class="nav-item active">
          <a class="nav-link mt-2" href="<?= base_url('/dashboard') ?>">
            <span class="fa fa-lg fa-home"></span>
            <span>الرئيسية</span>
          </a>
        </li>
        <hr>

           <!-- show log -->
           <li class="nav-item">
          <a class="nav-link" href="<?= base_url('/dashboard') ?>">
            <i class="fas fa-lg fa-database"></i>
            <span>بيانات الطلاب</span>
          </a>
        </li>

        <!-- penalty -->
        <li class="nav-item">
          <a class="nav-link" href="#">
            <i class="fas fa-lg fa-money-bill-1-wave"></i>
            <span>التغريم</span>
            <span class="group-icon float-end">
              <i class="fi fi-arrow-end"></i>
              <i class="fi fi-arrow-down fi-arrow-down"></i>
            </span>
          </a>
          <ul class="nav flex-column">
            <li class="nav-item n"><a class="nav-link" href="<?= base_url('penalty-single') ?>"><i class="fa fa-user"></i> إفرادي</a></li>
            <li class="nav-item n"><a class="nav-link" href="<?= base_url('penalty-collective') ?>"><i class="fa fa-users"></i> جماعي</a></li>
          </ul>
        </li>

        <!-- show log -->
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('/log-system'); ?>">
            <i class="fas fa-lg fa-file-alt"></i>
            <span>استعراض LOG</span>
          </a>
        </li>


        <!-- import from mobile -->
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('import-student') ?>">
            <i class="fas fa-lg fa-file-import"></i>
            <span class="small">استيراد البيانات من التطبيق</span>
          </a>
        </li>

        <!-- Users and Permessions -->
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('/users-list') ?>">
            <i class="fa fa-users fa-lg"></i>
            <span>المستخدمين والسماحيات</span>
          </a>
        </li>

        <!-- Units -->
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('/housing-units') ?>">

            <i class="fa fa-building fa-lg"></i>
            <span> الوحدات السكنية</span>
          </a>
        </li>

        <!-- Units -->
        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('/suites') ?>">

            <i class="fa fa-building fa-lg"></i>
            <span>الأجنحة</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?= base_url('/change-password') ?>">
            <i class="fa fa-star-of-life"></i>
            <span>تغيير كلمة السر</span>
          </a>
        </li>


        <hr>

        <!-- Logout -->
        <li class="nav-item">
          <a class="nav-link text-danger" href="<?= base_url('/logout') ?>">
            <i class="fas fa-lg fa-sign-out"></i>
            <span>تسجيل الخروج</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>

</aside>
<!-- /sidebar -->