<?= $this->extend('layouts/master_2'); ?>

<?= $this->section('content'); ?>


<div class="box mt-2">
    <?= form_open('penalty-collective', ['id' => 'collective-penalty-form']);   ?>

    <div class="box_header">
        <div class="box_title">تغريم جماعي</div>
        <div>
            <button type="submit" class="mybutton btn-light p-2" id="save" value="حفظ العملية">حفظ العملية</button>
            <button type="reset" class="mybutton  btn-light p-2" id="new" value="جديد">جديد</button>
        </div>
    </div>
    
    <div class="box_body">
        <input type="hidden" name="penalty_id" id="penalty_id">
        <div class="row">
            <div class="col-4 group">
                <span class="required">*</span>
                <label for="unit_number">الوحدة :</label>
                <select name="unit_number" id="unit_number" class="form-control">
                    <option value="" selected disabled>اختر رقم الوحدة</option>
                            <?php 
                            foreach($units as $un)
                            {
                                ?>
                                <option value="<?= $un['id'] ?>"><?= $un['name'] ?></option>
                                <?php
                            }
                            ?>
                </select>
            </div>
            <div class="col-4 group">
                <span class="required">*</span>
                <label for="suit_number">الجناح :</label>
                <select name="suit_number" id="suit_number" class="form-control ">
                    <option value="" disabled selected>اختر الجناح</option>
                   
                </select>
            </div>
            <div class="col-4 group">
                <span class="required">*</span>
                <label for="penalty_value">قيمة التغريم :</label>
                <input type="number" name="penalty_value" id="penalty_value" class="form-control" placeholder="ادخل قيمة التغريم">
            </div>
        </div>
        <div class="row">
            <div class="col-3 group">
                <label for="supervisor_book_date">تاريخ كتاب المشرف :</label>
                <input type="date" class="form-control" name="supervisor_book_date" id="supervisor_book_date">
            </div>
            <div class="col-3 group">
                <label for="supervisor_book_number">رقم كتاب المشرف :</label>
                <input type="number" class="form-control" name="supervisor_book_number" id="supervisor_book_number" placeholder="رقم كتاب المشرف">
            </div>
            <div class="col-3 group">
                <label for="order_id">رقم الطلب :</label>
                <input type="text" class="form-control" name="order_id" id="order_id" placeholder="رقم الطلب">
            </div>
            <div class="col-3 group">
                <label for="order_date">تاريخ الطلب :</label>
                <input type="date" class="form-control" name="order_date" id="order_date">
            </div>
        </div>
        <div class="row">
            <div class="col-6 group">
                <label for="penalty_reason">سبب التغريم :</label>
                <textarea name="penalty_reason" id="penalty_reason" rows="2" class="form-control" placeholder="ادخل سبب التغريم"></textarea>
            </div>
            <div class="col-6 group">
                <label for="panalty_details">تفاصيل التغريم :</label>
                <textarea name="panalty_details" id="panalty_details" rows="2" class="form-control" placeholder="ادخل تفاصيل التغريم"></textarea>
            </div>
        </div>

    </div>

    <?= form_close(); ?>

</div>

<br>

<!-- Search Box -->
<div class="box">
    <div class="row">
        <div class="col-md-11">
            <label for="text_search">البحث ب رقم الغرامة </label>
            <input type="text" name="text_search" id="text_search" class="form-control mt-1" placeholder="ادخل رقم الغرامة">
        </div>
        <div class="col-md-1">
            <button type="submit" id="search_btn" class="mybutton btn-warning mt-4">بحث</button>
        </div>
    </div>
</div>

<!-- Table Box for New Data -->
<table class="table table-bordered box">
    <caption class="caption-table caption-top shadow">لائحة التغريمات الجماعية</caption>
    <thead class="bg-warning">
        <th>#</th>
        <th>الوحدة</th>
        <th>الجناح</th>
        <th>قيمة التغريم</th>
        <th>تاريخ كتاب المشرف</th>
        <th>سبب التغريم</th>
        <th>عمليات</th>
    </thead>
    <tbody id="collective-penalty-table">
        <div class="loader">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </tbody>
</table>


<?= script_tag('public/assets/js/penalty_collective.js');  ?>


<?= $this->endSection(); ?>