<!-- Modal -->
<div class="modal fade" id="add_edit_suite" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?= form_open('add-edit-suite' , ['id' => 'new-suite-form'])  ?>
            <div class="modal-header bg-warning">
                <h5 class="modal-title" id="staticBackdropLabel">إضافة جناح جديد</h5>
            </div>
            <div class="modal-body">
                <input type="hidden" name="suite_id" id="suite_id">
                <div class="row group">
                    <div class="col-6">
                        <span class="required">*</span>
                        <label for="suite_name">اسم الجناح :</label>
                        <input type="text" name="suite_name" id="suite_name" class="form-control" placeholder="ادخل اسم الجناح">
                    </div>
                    <div class="col-6">
                        <span class="required">*</span>
                        <label for="unit_id">الوحدة :</label>
                        <select name="unit_id" id="unit_id" class="form-control">
                            <option disabled selected>اختر الوحدة</option>
                            <?php
                            foreach($units as $un)
                            {
                               echo  '<option value="'.$un['id'].'">'.$un['name'].'</option>';    
                            }
                            ?>
                        </select>
                    </div>

                </div>
                <div class="row group">
                    <div class="col-6">
                        <span class="required">*</span>
                        <label for="suite_first_room">رقم أول غرفة :</label>
                        <input type="number" name="suite_first_room" id="suite_first_room" class="form-control" placeholder="ادخل رقم آخر غرفة">
                    </div>
                    <div class="col-6">
                        <span class="required">*</span>
                        <label for="suite_last_room">رقم آخر غرفة :</label>
                        <input type="number" name="suite_last_room" id="suite_last_room" class="form-control" placeholder="ادخل رقم أول غرفة">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="mybutton btn-secondary" data-bs-dismiss="modal">إلغاء</button>
                <button type="submit" id="save" class="mybutton btn-warning">إضافة</button>
            </div>
        </div>
    </div>
</div>