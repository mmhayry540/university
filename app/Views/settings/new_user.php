<!-- Modal -->
<div class="modal fade" id="add_edit_user" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?= form_open('/add-edit-user', ['id' => 'new-user-form']) ?>
            <div class="modal-header bg-warning">
                <h5 class="modal-title" id="staticBackdropLabel">إضافة مستخدم جديد</h5>
            </div>
            <div class="modal-body">
                <input type="hidden" name="user_id" id="user_id">
                <div class="row group">
                    <div class="col-8">
                        <span class="required">*</span>
                        <label for="user_fullname">الاسم الثلاثي :</label>
                        <input type="text" name="user_fullname" id="user_fullname" class="form-control" placeholder="ادخل اسم المستخدم">
                    </div>
                    <div class="col-4">
                        <label for="user_active">حالة الحساب :</label>
                        <div class="d-flex justify-content-around align-items-center">
                            <div class="">
                                <input type="radio" name="user_active" id="user_active_yes" class="form-check-input  mx-2" value="1" checked>
                                <label for="user_active_yes">
                                <span class="badge bg-success">فعال</span>
                                </label>
                            </div>
                            <div class="">
                                <input type="radio" name="user_active" id="user_active_no" class="form-check-input mx-2" value="0">
                                <label for="user_active_no">
                                    <span class="badge bg-danger">غير فعال</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row group">
                    <div class="col-6" id="update-username">
                        <span class="required">*</span>
                        <label for="user_name">اسم المستخدم :</label>
                        <input type="text" name="user_name" id="user_name" class="form-control" placeholder="ادخل اسم المستخدم">
                    </div>
                    <div class="col-6" id="update-password">
                        <span class="required">*</span>
                        <label for="user_password">كلمة السر :</label>
                        
                        <div class="input-group mb-3">
                            <span class="input-group-text" id="show-password" onclick="ShowHidePassword('user_password')">
                                <i class="fa fa-eye"></i>
                            </span>
                            <input type="password" name="user_password" id="user_password" class="form-control" placeholder="ادخل كلمة السر" autocomplete>
                        </div>

                    </div>
                </div>
                <div class="row group">
                    <div class="col-12">
                        <label for="Permissions">السماحيات :</label>
                        <div class="d-flex justify-content-between flex-wrap align-items-center bg-lime border border-1 p-3">
                            <div class="w-50">
                                <input type="checkbox" name="add_penalty" id="add_penalty" value="1" class="form-check-input">
                                <label for="add_penalty">تغريم</label>
                            </div>
                            <div  class="w-50">
                                <input type="checkbox" name="cut_payment" id="cut_payment" value="1" class="form-check-input">
                                <label for="cut_payment">قطع إيصال دفع</label>
                            </div>
                            <div  class="w-50">
                                <input type="checkbox" name="confirm_payment" id="confirm_payment" value="1" class="form-check-input">
                                <label for="confirm_payment">تثبيت إيصال دفع</label>
                            </div>
                            <div class="w-50">
                                <input type="checkbox" name="users_permessions" id="users_permessions" value="1" class="form-check-input">
                                <label for="users_permessions">المستخدمين والسماحيات</label>
                            </div>
                            <div  class="w-50">
                                <input type="checkbox" name="emport_from_excel" id="emport_from_excel" value="1" class="form-check-input">
                                <label for="emport_from_excel">إستيراد من الإكسل</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="mybutton btn-secondary" data-bs-dismiss="modal">إلغاء</button>
                <button type="submit" id="save" class="mybutton btn-warning">إضافة</button>
            </div>
            <?= form_close()  ?>
        </div>
    </div>
</div>