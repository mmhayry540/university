<?= $this->extend('layouts/master_2'); ?>

<?= $this->section('content'); ?>



<div class="box w-75 mx-auto">
<?php if (session()->has('error')) : ?>
    <div class="alert alert-danger">
        <?= session()->get('error') ?>
    </div>
<?php endif; ?>
<?php if (session()->has('success')) : ?>
    <div class="alert alert-success">
        <?= session()->get('success') ?>
    </div>
<?php endif; ?>
<?= form_open('/change-password', ['method' => 'post']) ?>
<?= csrf_field() ?>
    <div class="row group">
        <div class="col-12">
            <span class="required">*</span>
            <label for="currentpass">كلمة السر الحالية :</label>
            <input type="password" name="currentpass" id="currentpass" class="form-control" placeholder="ادخل كلمة السر الحالية">
        </div>
    </div>
    <div class="row group">
        <div class="col-12">
            <span class="required">*</span>
            <label for="newpassword">كلمة السر الجديدة :</label>
            <input type="password" name="newpassword" id="newpassword" class="form-control" placeholder="ادخل كلمة السر الجديدة">
        </div>
    </div>
    <div class="row group">
        <div class="col-12">
            <span class="required">*</span>
            <label for="">تأكيد كلمة السر الجديدة :</label>
            <input type="password" name="confirmpass" id="confirmpass" class="form-control" placeholder="ادخل تأكيد كلمة السر الجديدة">
        </div>
    </div>
    <hr>
    <div class="text-center">
        <input type="submit" value="حفظ التغيير" class="mybutton btn-warning">
       
    </div>
    <?= form_close(); ?>
    
</div>

<?= $this->endSection(); ?>