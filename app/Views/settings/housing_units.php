<?= $this->extend('layouts/master_2'); ?>

<?= $this->section('content'); ?>

<div class="box">
    <div class="mybutton btn-warning w-25" id="ModalBTN" data-bs-toggle="modal" data-bs-target="#add_edit_unit">وحدة سكنية جديدة</div>
</div>

<br>

<?= $units['pager']->links() ?>


<!-- Table Box for User List --> 
<table class="table table-bordered box">
    <caption class="caption-table caption-top shadow">لائحة الوحدات السكنية</caption>
    <thead class="bg-warning">
        <th>#</th>
        <th>اسم الوحدة</th>
        <th>اسم المشرف/ة</th>
        <th>النوع</th>
        <th width="12%">عمليات</th>
    </thead>
    <tbody id="units-table">
        <div class="loader">
            <div></div>
            <div></div>
            <div></div>
        </div>
      
      
    </tbody>
</table>

<?= $units['pager']->links() ?>


<!-- this include for modal new user -->
<?= $this->include("settings/new_unit");   ?>

<?= script_tag('public/assets/js/housing_units.js');  ?>


<?= $this->endSection(); ?>
