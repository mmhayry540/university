<!-- Modal -->
<div class="modal fade" id="add_edit_unit" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?= form_open('/add-edit-unit' , ['id'=>'new-unit-form']) ?>
            <div class="modal-header bg-warning">
                <h5 class="modal-title" id="staticBackdropLabel">إضافة وحدة سكنية جديدة</h5>
            </div>
            <div class="modal-body">
                <input type="hidden" name="unit_id" id="unit_id">
                <div class="row group">
                    <div class="col-4">
                        <span class="required">*</span>
                        <label for="unit_name">اسم الوحدة :</label>
                        <input type="text" name="unit_name" id="unit_name" class="form-control" placeholder="ادخل اسم الوحدة">
                    </div>
                    <div class="col-4">
                        <span class="required">*</span>
                        <label for="supervisor_name">اسم المشرف/ة :</label>
                        <input type="text" name="supervisor_name" id="supervisor_name" class="form-control" placeholder="ادخل اسم المشرف/ة">
                    </div>
                    <div class="col-4">
                        <span class="required">*</span>
                        <label for="unit_type">النوع :</label>
                        <select name="unit_type" id="unit_type" class="form-control">
                        <option value selected disabled>اختر النوع</option>    
                        <option value="M" >ذكور</option>    
                        <option value="F">إناث</option>    
                        <select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="mybutton btn-secondary" data-bs-dismiss="modal">إلغاء</button>
                <button type="submit" id="save" class="mybutton btn-warning">إضافة</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>