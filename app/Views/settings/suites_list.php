<?= $this->extend('layouts/master_2'); ?>

<?= $this->section('content'); ?>

<div class="box">
    <div class="mybutton btn-warning w-25" id="ModalBTN" data-bs-toggle="modal" data-bs-target="#add_edit_suite">جناح جديد</div>
</div>

<br>

<?= $suites['pager']->links(); ?>
<div class="pages"></div>

<!-- Table Box for User List --> 
<table class="table table-bordered box">
    <caption class="caption-table caption-top shadow">لائحة الأجنحة</caption>
    <thead class="bg-warning">
        <th>#</th>
        <th>اسم الجناح</th>
        <th>رقم أول غرفة</th>
        <th>رقم آخر غرفة</th>
        <th>الوحدة</th>
        <th width="12%">عمليات</th>
    </thead>
    <tbody id="suite-table">
        <div class="loader">
            <div></div>
            <div></div>
            <div></div>
        </div>
        <!-- <tr>
            <td>1</td>
            <td>الأولى</td>
            <td>3</td>
            <td>6</td>
            <td>الأولى</td>
            <td class="d-flex justify-content-around p-2">
                <div class="mybutton btn-warning" title="تعديل"><span class="fa fa-edit"></span></div>
            </td>
        </tr> -->
      
    </tbody>
</table>

<?= $suites['pager']->links(); ?>


<!-- this include for modal new user -->
<?= $this->include("settings/new_suite");   ?>

<?= script_tag('public/assets/js/suites_list.js');  ?>

<?= $this->endSection(); ?>
