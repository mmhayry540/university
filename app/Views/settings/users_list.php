<?= $this->extend('layouts/master_2'); ?>

<?= $this->section('content'); ?>

<div class="box">
    <div class="mybutton btn-warning w-25" id="ModalBTN" data-bs-toggle="modal"  data-bs-target="#add_edit_user">مستخدم جديد</div>
</div>

<br>

<?= $users['pager']->links() ;?>

<!-- Table Box for User List --> 
<table class="table table-bordered box">
    <caption class="caption-table caption-top shadow">لائحة المستخدمين</caption>
    <thead class="bg-warning">
        <th>#</th>
        <th>الاسم الثلاثي</th>
        <th>اسم المستخدم</th>
        <th>السماحيات</th>
        <th>حالة الحساب</th>
        <th>عمليات</th>
    </thead>
    <tbody id="users-table">
    <div class="loader">
            <div></div>
            <div></div>
            <div></div>
        </div>
     
      
    </tbody>
</table>

<?= $users['pager']->links() ;?>


<!-- this include for modal new user -->
<?= $this->include("settings/new_user");   ?>

<?= script_tag('public/assets/js/users_list.js');  ?>

<?= $this->endSection(); ?>
