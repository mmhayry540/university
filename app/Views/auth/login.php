<?= $this->extend('layouts/master'); ?>

<?= $this->section('content'); ?>

<div class="login-page">

    <div class="login-image">
        <img src="<?= base_url('public/assets/images/login_image.jpg') ?>" alt="">
    </div>
    <div class="login-form">
        <?= form_open('login') ?>
        <?= csrf_field() ?>
        <div class="login-title">الهيئة العامة للمدينة الجامعية</div>
        <div class="login">
            <div class="login-text">تسجيل الدخول</div>
            <?php if (isset($validation)) : ?>
                            <div class="alert alert-danger"><?= $validation->getError('username') ?></div>
                        <?php endif; ?>
            <hr>
            <div class="group">
                <label for="username">اسم المستخدم :</label>
                <input type="text" name="username" id="username" class="form-control" placeholder="ادخل اسم المستخدم ...">
            </div>
            <div class="group">
                <label for="password">كلمة السر :</label>
                <input type="password" name="password" id="password" class="form-control" placeholder="ادخل كلمة السر ...">
            </div>
            <hr>
            <div class="login-footer">
                <input type="submit" value="تسجيل الدخول" class="btn btn-primary">
            </div>
        </div>
        <?= form_close(); ?>
    </div>

</div>

<?= $this->endSection(); ?>