<?= $this->extend('layouts/master_2'); ?>

<?= $this->section('content'); ?>
<?php //if (isset($userlogs)) {echo $userlogs;} ?>
<!-- Search Box -->
<div class="box">
    <?= form_open('/log-system') ?>
    <div class="row">
        <div class="col-md-4">
            <label for="search_start_date">البحث من تاريخ :</label>
            <input type="date" name="search_start_date" id="search_start_date" class="form-control mt-1">
        </div>
        <div class="col-md-4">
            <label for="search_end_date">إلى تاريخ</label>
            <input type="date" name="search_end_date" id="search_end_date" class="form-control mt-1">
        </div>
        <div class="col-md-3">
            <label for="search_user">حسب المستخدم</label>
            <select name="search_user" id="search_user" class="form-control mt-1">
                <option value="0" selected>الكل</option>
                <?php
                    foreach($users as $u)
                    {
                    ?>
                <option value="<?= $u['id'] ?>"><?= $u['name'] ?></option>



                <?php
                    }
                    ?>
            </select>
        </div>
        <div class="col-md-1">
            <button type="submit" id="search_btn" class="mybutton btn-warning mt-4">بحث</button>
        </div>
    </div>
    <?= form_close()  ?>
</div>

<hr>
<table class="table table-bordered box">
    <caption class="caption-table caption-top shadow">لائحة نظام LOG</caption>
    <thead class="bg-warning">
        <th>#</th>
        <th>العملية</th>
        
        <th>المستخدم</th>
        <th>التاريخ</th>
    </thead>
    <tbody id="New-Data-table">
    <?php
          foreach($userlogs as $userlog)
                    {
                    ?>
                    <tr>
                    <td> <?= $userlog['id'] ?></td>
                    <td> <?= $userlog['action'] ?></td>
                    
                    <td> <?= $userlog['user_id'] ?></td>
                    <td> <?= $userlog['activity_date'] ?></td>
                    </tr>

                <?php
                    }
                    ?>
    </tbody>
</table>




<?= $this->endSection(); ?>