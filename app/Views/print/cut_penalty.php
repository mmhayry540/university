<!DOCTYPE html>
<html lang="ar">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= link_tag('public/core/css/core.css'); ?>
    <?= link_tag('public/assets/css/print.css'); ?>
    <title><?= $title ?></title>
</head>
<pre>
<?php
$student_name = $student->fullname;
$student_mother = $student->mother;
$student_date = $student->study_year;
$univercity = $student->faculty;
$penalty_value = $penalt->value;
$unit = GetNameUnitById($penalt->housebuilding_id);
$room =$student->roomno;
if( $penalt->ptype == 0){$penalty_type = 'تغريم إفرادي';}
elseif( $penalt->ptype == 1){$penalty_type = 'تغريم جماعي';}
$penalty_details = $penalt->details;

?>
</pre>
<body style="padding: 10px 35px">

    <div class="sect" id="univercity_version">
        <div class="header">
            <div class="top-right">
                <div>الهيئة العامة للمدينة الجامعية بحلب</div>
            </div>
            <div class="top-center">
                <div>
                    <span>إلى السيد محاسب الهيئة العامة للمدينة الجامعية</span>
                </div>
            </div>
        </div>
        <div class="content fw-medium" style="word-spacing: 2px; ">
            <span>يرجى قبض مبلغ وقدره </span> <span class="underline"><?= $penalty_value; ?> </span> ل.س من الطالب <span class="underline"> <?= $student_name ?> </span>اسم الأم <span class="underline"> <?= $student_mother; ?> </span> تاريخ الولادة <span class="underline"> <?= $student_date; ?> </span> المقيم في الوحدة <span class="underline"><?= $unit ;?></span>
             في الغرفة <span class="underline"><?= $room; ?></span> في الكلية <span class="underline"> <?= $univercity; ?> </span>
            <span>وذلك لقاء عقوبة تغريم</span> <span class="underline"><?= $penalty_type; ?> </span> وذلك بسبب <span class="underline"> <?= $penalty_details; ?> </span>
            <br>
            <span>إلى وزارة شؤون الطلاب بالمدينة الجامعية سكن (صيفي / شتوي)</span> 
            <br>
            <span>جرى قبض المبلغ المذكور من قبلي </span>
            <div class="d-flex justify-content-between p-3">
                <div class="right">
                    <u>المعتمد</u>
                </div>
                <div class="left">
                    <span class="small"> نسخة شؤون الطلاب</span>
                </div>
            </div>
            <br>
        </div>
    </div>
    <hr>
    <div class="sect" id="thuery_version">
        <div class="header">
            <div class="top-right">
                <div>الهيئة العامة للمدينة الجامعية بحلب</div>
            </div>
            <div class="top-center">
                <div>
                    <span>إلى السيد محاسب الهيئة العامة للمدينة الجامعية</span>
                </div>
            </div>
        </div>
        <div class="content fw-medium" style="word-spacing: 2px; ">
        <span>يرجى قبض مبلغ وقدره </span> <span class="underline"><?= $penalty_value; ?> </span> ل.س من الطالب <span class="underline"> <?= $student_name ?> </span>اسم الأم <span class="underline"> <?= $student_mother; ?> </span> تاريخ الولادة <span class="underline"> <?= $student_date; ?> </span> المقيم في الوحدة <span class="underline"><?= $unit ;?></span>
             في الغرفة <span class="underline"><?= $room; ?></span> في الكلية <span class="underline"> <?= $univercity; ?> </span>
            <span>وذلك لقاء عقوبة تغريم</span> <span class="underline"><?= $penalty_type; ?> </span> وذلك بسبب <span class="underline"> <?= $penalty_details; ?> </span>
            <br>
            <span>إلى وزارة شؤون الطلاب بالمدينة الجامعية سكن (صيفي / شتوي)</span> 
            <br>
            <span>جرى قبض المبلغ المذكور من قبلي </span>
            <div class="d-flex justify-content-between p-3">
                <div class="right">
                    <u>المعتمد</u>
                </div>
                <div class="left">
                    <span class="small"> نسخة المحاسبة</span>
                </div>
            </div>
            <br>
        </div>
    </div>
    <hr>
    <div class="sect" id="student_version">
        <div class="header">
            <div class="top-right">
                <div>الهيئة العامة للمدينة الجامعية بحلب</div>
            </div>
            <div class="top-center">
                <div>
                    <span>إلى السيد محاسب الهيئة العامة للمدينة الجامعية</span>
                </div>
            </div>
        </div>
        <div class="content fw-medium" style="word-spacing: 2px; ">
        <span>يرجى قبض مبلغ وقدره </span> <span class="underline"><?= $penalty_value; ?> </span> ل.س من الطالب <span class="underline"> <?= $student_name ?> </span>اسم الأم <span class="underline"> <?= $student_mother; ?> </span> تاريخ الولادة <span class="underline"> <?= $student_date; ?> </span> المقيم في الوحدة <span class="underline"><?= $unit ;?></span>
            في الغرفة <span class="underline"><?= $room; ?></span> في الكلية <span class="underline"> <?= $univercity; ?> </span>
            <span>وذلك لقاء عقوبة تغريم</span> <span class="underline"><?= $penalty_type; ?> </span> وذلك بسبب <span class="underline"> <?= $penalty_details; ?> </span>
            <br>
            <span>إلى وزارة شؤون الطلاب بالمدينة الجامعية سكن (صيفي / شتوي)</span> 
            <br>
            <span>جرى قبض المبلغ المذكور من قبلي </span>
            <div class="d-flex justify-content-between p-3">
                <div class="right">
                    <u>المعتمد</u>
                </div>
                <div class="left">
                    <span class="small"> نسخة الطالب </span>
                </div>
            </div>
            <br>
        </div>
    </div>
    


</body>

</html>