<!DOCTYPE html>
<html lang="ar">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= link_tag('public/core/css/core.css'); ?>
    <?= link_tag('public/assets/css/print.css'); ?>
    <title><?= $title ?></title>
</head>



<body>

    <div class="header">
        <div class="top-right">
            <div>الجمهورية العربية السورية</div>
            <div>وزارة التعليم العالي والبحث العلمي</div>
            <div>الهيئة العامة للمدينة الجامعية</div>
        </div>
        <div class="top-center">
            <div class="fw-bold">
                <span><i>مذكرة إدارية رقم</i></span>
                /<span class=" mx-2"><?= ($penalty['orderid'] != '') ?  $penalty['orderid']  : '' ?></span>/
            </div>
        </div>
    </div>
    <div class="content">
        <div class="conditions">
            <div class="cond">- بناءً على أحكام قانون رقم (29) لعام (2022).</div>
            <div class="cond">- بناءً على أحكام النظام الداخلي للمدن للجامعية رقم (194 / و) تاريخ 13 / 10 / 2001 .</div>
            <div class="cond">- واستناداً لجدول التغريم للعام الدراسي (2022 / 2023) .</div>
            <div class="cond">- واستناداً إلى كتاب مشرف<?php if ($type == 'F') {
                                                            echo "ة";
                                                        } ?> الوحدة السكنية / <span><?= ($penalty['housebuilding_id'] != '') ?  $penalty['housebuilding_id']  : '' ?></span> / بتاريخ <span><?= ($penalty['supervisor_orderdate'] != '') ?  date('d / m / Y', strtotime($penalty['supervisor_orderdate'])) : '' ?></span> م.</div>
            <div class="cond">- وبناءً على مقتضيات المصلحة العامة .</div>
        </div>
        <big>
            <div class="text-center fw-bold my-2">يعمل ما يلي : </div>
        </big>

        <div class="mad">

            <label for="" class="fw-bold"><big>مادة 1 -</big></label>
            <span><?php
                    if ($type == 'F') {
                        echo "تغرم الطالبات المقيمات";
                    } else {
                        echo 'يغرم الطلاب المقيمين';
                    } ?>
            </span>في الغرفة / <span id="room"><?= ($penalty['roomno'] != '') ?  $penalty['roomno']  : '' ?></span> /
            في الوحدة السكنية / <span id="unit"><?= ($penalty['housebuilding_id'] != '') ?  $penalty['housebuilding_id']  : '' ?></span> /
            بمبلغ / <span id="value"><?= ($penalty['value'] != '') ?  $penalty['value']  : '' ?></span> / ل.س
            <span id="value-text"></span> فقط لا غير
            بالتضامن و التكافل فيما بين<?php if ($type == 'F') {
                                            echo 'هن';
                                        } else {
                                            echo 'هم';
                                        } ?>
            وذلك بسبب <span id="cause"><?= ($penalty['cause'] != '') ?  $penalty['cause']  : '' ?></span>
            <span id="details"><?= ($penalty['details'] != '') ?  $penalty['details']  : '' ?></span>
            <br>
            وهن :
            <br>
            <div id="students" class="mx-4 my-2">

                <?php
                $count = 1;
                foreach ($students as $st) {
                ?>

                    <div class="student p-1"><?= $count++ ?> -
                        <span id="fullname"><?= ($st['fullname'] != '') ?  $st['fullname']  : '' ?></span>
                        من <?= ($type == 'F') ? 'طالبات' : 'طلاب' ?> كلية <span id="faculty"><?= ($st['faculty'] != '') ?  $st['faculty']  : '' ?></span>.
                    </div>

                <?php
                }
                ?>
            </div>
        </div>

        <div class="mad">

            <label for="" class="fw-bold"><big>مادة 2 -</big></label>
            <span>يتم تسديد مبلغ التغريم المطلوب إلى معتمد الرسوم في المدينة الجامعية .</span>
        </div>

        <div class="mad">

            <label for="" class="fw-bold"><big>مادة 3 -</big></label>
            <span>يعمل بهذا الأمر الإداري و يبلغ من يلزم لتنفيذه اعتباراً من تايخه .</span>
        </div>

        <div class="d-flex justify-content-between align-items-center p-3">
            <div class="d-flex flex-column fw-bold">
                <span>مشرف<?php if ($type == 'F') {
                                echo 'ة';
                            } ?> الوحدة</span>
                <span><?= ($supervisor != '') ? $supervisor : ''; ?></span>
            </div>
            <div class="d-flex flex-column fw-bold">
                <span>محاسبة المواد</span>
                <span>شذى قاسم</span>
            </div>
            <div class="d-flex flex-column text-center fw-bold">
                <span>المدير العام للهيئة العامة للمدينة الجامعية</span>
                <span>د. م. دارين الكعدي</span>
            </div>
        </div>

    </div>
    <div class="footer">
        <div class="fw-bold">صورة إلى :</div>
        <span>- مشرف الوحدة / <span id="unit"><?= ($penalty['housebuilding_id'] != '') ?  $penalty['housebuilding_id']  : '' ?></span> / .</span>
        <span>- محاسبة المواد .</span>
        <span>- ديوان الإقامة / براءة الذمة .</span>
        <span>- الديوان العام .</span>
        <span></span>
    </div>

</body>

</html>