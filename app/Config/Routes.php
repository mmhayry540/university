<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
//$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/NewStudentsList', 'Home::NewStudentsList');
$routes->get('/OldStudentsList', 'Home::OldStudentsList');
$routes->get('/get_student_data', 'Home::get_student_data');
$routes->get('/search', 'Home::search');


$routes->match(['get' , 'post'] , '/login' , 'Home::login');
$routes->match(['get' , 'post'] , '/logout' , 'Home::logout');
$routes->match(['get' , 'post'] , '/pass' , 'Home::pass');
$routes->match(['get' , 'post'] , '/testresult' , 'Home::testresult');
$routes->match(['get' , 'post'] ,'/dashboard', 'Home::dashboard');
$routes->match(['get' , 'post'] ,'/cut_penalty/(:num)/(:num)/(:num)/(:num)', 'Home::cut_penalty/$1/$2/$3/$4');
$routes->match(['get' , 'post'] ,'/confirm_penalty', 'Home::confirm_penalty');
$routes->match(['get' , 'post'] ,'/confirm_penalty_old', 'Home::confirm_penalty_old');



// penalty
$routes->match(['get' , 'post'] , '/penalty-single' , 'Penalty::penalty_single');
$routes->match(['get' , 'post'] , '/penalty-collective' , 'Penalty::penalty_collective');
$routes->get('/penalty_list' , 'Penalty::penalty_list');
$routes->get('/get_penalty_data_by_id' , 'Penalty::get_penalty_data_by_id');
$routes->get('/get_students_by_room_id' , 'Penalty::get_students_by_room_id');
$routes->get('/get_suites_by_unit_id' , 'Penalty::get_suites_by_unit_id');
$routes->get('/print_single_penalty/(:num)' , 'Penalty::print_single_penalty/$1');
$routes->get('/print_collective_penalty/(:num)' , 'Penalty::print_collective_penalty/$1');

$routes->match(['get' , 'post'] ,'/add_payment_penalty', 'Penalty::add_payment_penalty');


// Log system 
$routes->match(['get' , 'post'] ,'/log-system' , 'LogActivity::log_system');


// settings - Users
$routes->get('/users-list' , 'Setting::users_list');
$routes->get('/getUsers' , 'Setting::getUsers');
$routes->post('/add-edit-user' , 'Setting::add_edit_user');
$routes->get('/get_user_data_by_id' , 'Setting::get_user_data_by_id');

// settings - Units
$routes->get('/housing-units' , 'Setting::housing_units');
$routes->get('/getUnits' , 'Setting::getUnits');
$routes->get('/get_unit_data_by_id' , 'Setting::get_unit_data_by_id');
$routes->post('/add-edit-unit' , 'Setting::add_edit_unit');
$routes->get('/get_unit_name_by_id' , 'Setting::get_unit_name_by_id');


// settings - Suites
$routes->get('/suites' , 'Setting::suites');
$routes->get('/getSuite' , 'Setting::getSuite');
$routes->get('/get_suite_data_by_id' , 'Setting::get_suite_data_by_id');
$routes->post('/add-edit-suite' , 'Setting::add_edit_suite');

// settings - change password
$routes->match(['get' , 'post'] ,'/change-password' , 'Setting::change_password');

// 
//------------- Import Excel File --------------
$routes->get('import-excel', 'ImportExcel::index');
$routes->post('import-excel/upload', 'ImportExcel::upload');
$routes->get('import-student', 'ImportStudents::index');
$routes->post('import-student/upload', 'ImportStudents::upload');
//-------------
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
