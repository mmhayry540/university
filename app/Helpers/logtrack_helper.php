<?php

// firstly to load this helper you need add name in Helpers in BaseControllers; 

if (!function_exists('log_user_activity')) {
    function log_user_activity($action)
    {
        $userid = session()->get('id');
        $logModel = new \App\Models\UserActivityLog_Model();

        $data = [
            'user_id'   => $userid ?? 0,
            'action'    => $action,
            'ip_address'=> $_SERVER['REMOTE_ADDR'],
            'user_agent'=> $_SERVER['HTTP_USER_AGENT'],
        ];

        $logModel->insert($data);
    }
}
