<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddTriggersDeleteToStudentsPenalty extends Migration
{
    public function up()
    {
       $this->db->query("
CREATE TRIGGER `after_penalty_delete` AFTER DELETE ON `studentpenalty`
 FOR EACH ROW BEGIN
    DECLARE current_status INT DEFAULT 0;

    -- Get the current status sum for the student
    SELECT IFNULL(SUM(status), 0) INTO current_status 
    FROM studentpenalty 
    WHERE stdid = OLD.stdid;

    -- Check if the current status after deletion would still be positive
    IF (current_status - OLD.status) > 0 THEN
        UPDATE students
        SET status = 1
        WHERE id = OLD.stdid;
    ELSE
        -- Set student status to 0 if no penalties remain
        UPDATE students
        SET status = 0
        WHERE id = OLD.stdid;
    END IF;
END

       ");

      
    }

    public function down()
    {
        //$this->forge->dropColumn('users', 'per_users');
        $this->db->query("DROP TRIGGER IF EXISTS after_penalty_delete");
        
    }
}