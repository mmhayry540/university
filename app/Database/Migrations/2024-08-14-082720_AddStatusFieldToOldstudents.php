<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddStatusFieldToOldstudents extends Migration
{
    public function up()
    {
        $this->forge->addColumn('oldstudents0', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 1,
            ],
        ]);
        $this->forge->addColumn('oldstudents1', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 1,
            ],
        ]);
        $this->forge->addColumn('oldstudents2', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 1,
            ],
        ]);
        $this->forge->addColumn('oldstudents3', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 1,
            ],
        ]);
        $this->forge->addColumn('oldstudents4', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 1,
            ],
        ]);
        $this->forge->addColumn('oldstudents5', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 1,
            ],
        ]);
        $this->forge->addColumn('oldstudents6', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 1,
            ],
        ]);
        $this->forge->addColumn('oldstudents7', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 1,
            ],
        ]);
        $this->forge->addColumn('oldstudents8', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 1,
            ],
        ]);
        $this->forge->addColumn('oldstudents9', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 1,
            ],
        ]);
        $this->forge->addColumn('oldstudents10', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 1,
            ],
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('oldstudents0', 'status');
        $this->forge->dropColumn('oldstudents1', 'status');
        $this->forge->dropColumn('oldstudents2', 'status');
        $this->forge->dropColumn('oldstudents3', 'status');
        $this->forge->dropColumn('oldstudents4', 'status');
        $this->forge->dropColumn('oldstudents5', 'status');
        $this->forge->dropColumn('oldstudents6', 'status');
        $this->forge->dropColumn('oldstudents7', 'status');
        $this->forge->dropColumn('oldstudents8', 'status');
        $this->forge->dropColumn('oldstudents9', 'status');
        $this->forge->dropColumn('oldstudents10', 'status');

    }
}