<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddStatusFieldToStudentPenalty extends Migration
{
    public function up()
    {
        $this->forge->addColumn('studentpenalty', [
            'status' => [
                'type'       => 'tinyint',
                'constraint' => '1',
            ],
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('studentpenalty', 'status');
    }
}