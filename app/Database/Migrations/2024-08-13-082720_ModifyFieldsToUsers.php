<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ModifyFieldsToUsers extends Migration
{
    public function up()
    {
        $this->forge->modifyColumn('users', [
            'per_addpenalty' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => '0',
            ],
            'per_cutpayment' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => '0',
            ],
            'per_confpayment' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => '0',
            ],
        ]);
    }

    public function down()
    {

    }
}