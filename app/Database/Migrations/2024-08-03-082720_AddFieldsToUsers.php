<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddFieldsToUsers extends Migration
{
    public function up()
    {
        $this->forge->addColumn('users', [
            'per_addpenalty' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                
            ],
            'per_cutpayment' => [
                'type'       => 'tinyint',
                'constraint' => '1',
            ],
            'per_confpayment' => [
                'type'       => 'tinyint',
                'constraint' => '1',
            ],
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('users', 'per_addpenalty');
        $this->forge->dropColumn('users', 'per_cutpayment');
        $this->forge->dropColumn('users', 'per_confpayment');
    }
}