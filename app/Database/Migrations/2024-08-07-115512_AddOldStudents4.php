<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddOldStudents4 extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'NationalID' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'study_year' => [
                'type'       => 'VARCHAR',
                'constraint' => '20',
                'null' => true,
            ],
            'study_season' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'fullname' => [
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'father' => [
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'mother' => [
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
           'faculty' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'student_id' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'partname' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ],
            'year' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'city' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ],
            'birthdate_place' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ],
            'kaid' => [
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'address' => [
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'building_id' => [
                'type'       => 'VARCHAR',
                'constraint' => '10',
                'null' => true,
            ],
            'room_id' => [
                'type'       => 'VARCHAR',
                'constraint' => '20',
                'null' => true,
            ],
            'penalty_detail' => [
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'penalty_date' => [
                'type'       => 'VARCHAR',
                'constraint' => '20',
                'null' => true,
            ],
            'penalty_value' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'order_id' => [
                'type'       => 'VARCHAR',
                'constraint' => '10',
                'null' => true,
            ],
            'order_date' => [
                'type'       => 'VARCHAR',
                'constraint' => '20',
                'null' => true,
            ],
            'order_notice1' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'order_notice2' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
        ]);
        $this->forge->addKey('id', true);
        //$this->forge->addIndex('[id', '[id]');
        $this->forge->createTable('oldstudents4');
    }

    public function down()
    {
        $this->forge->dropTable('oldstudents4');
    }
}