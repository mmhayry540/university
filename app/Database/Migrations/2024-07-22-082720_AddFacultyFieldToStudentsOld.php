<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddFacultyFieldToStudentsOld extends Migration
{
    public function up()
    {
        $this->forge->addColumn('studentsold', [
            'faculty' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null'       => true,
            ],
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('studentsold', 'faculty');
    }
}