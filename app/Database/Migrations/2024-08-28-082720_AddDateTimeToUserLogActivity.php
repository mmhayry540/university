<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddDateTimeToUserLogActivity extends Migration
{
    public function up()
    {
        $this->forge->addColumn('user_activity_logs', [
            'activity_date' => [
                'type' => 'DATETIME',
                'default' => date('Y-m-d H:i:s'), 
            ],
           
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('user_activity_logs', 'activity_date');
    }
}