<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddTriggersToStudentsPenalty extends Migration
{
    public function up()
    {
       $this->db->query("
        CREATE TRIGGER `after_penalty_insert` AFTER INSERT ON `studentpenalty`
        FOR EACH ROW BEGIN
            DECLARE current_status INT;
           

    -- Get the current state for the studentpenalty for cuurent stednet
    SELECT sum(status) INTO current_status FROM studentpenalty WHERE stdid = NEW.stdid ;
    -- Ensure there's enough stock
    IF (current_status + NEW.status) > 0  THEN
		UPDATE students
        SET status = 1
        WHERE id = NEW.stdid;
    ELSE
        -- Reduce the stock by the quantity ordered
      	UPDATE students
        SET status = 0
        WHERE id = NEW.stdid;
    END IF;
END
       ");

       $this->db->query("
       CREATE TRIGGER `after_penalty_update` AFTER UPDATE ON `studentpenalty`
    FOR EACH ROW BEGIN
     DECLARE current_status INT;

    -- Get the current state for the studentpenalty for cuurent stednet
    SELECT sum(status) INTO current_status FROM studentpenalty WHERE stdid = NEW.stdid ;

    -- Ensure there's enough stock
    IF (current_status +  NEW.status) > 0  THEN
		UPDATE students
        SET status = 1
        WHERE id = NEW.stdid;
    ELSE
        -- Reduce the stock by the quantity ordered
      	UPDATE students
        SET status = 0
        WHERE id = NEW.stdid;
    END IF;
END
       ");

    }

    public function down()
    {
        //$this->forge->dropColumn('users', 'per_users');
        $this->db->query("DROP TRIGGER IF EXISTS after_penalty_insert");
        $this->db->query("DROP TRIGGER IF EXISTS after_penalty_update");
        
    }
}