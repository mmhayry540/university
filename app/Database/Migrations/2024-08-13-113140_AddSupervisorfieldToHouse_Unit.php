<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddSupervisorfieldToHouse_Unit extends Migration
{
    public function up()
    {
        $this->forge->addColumn('house_unit', [
            'supervisor' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => null,
            ],
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('house_unit', 'supervisor');
    }
}