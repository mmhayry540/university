<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddPenaltySingals extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'housebuilding_id' => [ // الوحدة السكنية
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'roomno' => [ // رقم الغرفة
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'value' => [ // مقدار التغريم
                'type'       => 'INT',
                'constraint' => '10',
            ], 
            'cause' => [ // سبب التغريم
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ], 
            'details' => [ // تفاصيل التغريم
                'type'       => 'TEXT',
                'null' => true,
                
            ], 
            'supervisor_orderid' => [ // رقم كتاب المشرف
                'type'       => 'VARCHAR',
                'constraint' => '10',
                'null' => true,
            ],   
            'supervisor_orderdate' => [ // تاريخ كتاب المشرف
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'orderid' => [ // رقم المذكرة الإدارية
                'type'       => 'VARCHAR',
                'constraint' => '10',
                'null' => true,
            ],   
            'orderdate' => [ // تاريخ المذكرة الإدارية
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],           
           
           
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('penaltysingals');
    }

    public function down()
    {
        $this->forge->dropTable('penaltysingals');
    }
}