<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddFacultyFieldToBuildingdetails extends Migration
{
    public function up()
    {
        $this->forge->addColumn('buildingdetails', [
            'faculty' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null'       => true,
            ],
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('buildingdetails', 'faculty');
    }
}