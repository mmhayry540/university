<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddStatusFieldToStudentPenalty extends Migration
{
    public function up()
    {
        $this->forge->addColumn('studentpenalty', [
            'payid' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ],
            'paydate' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ],
        ]);
       
    }

    public function down()
    {
        $this->forge->dropColumn('studentpenalty', 'payid');
        $this->forge->dropColumn('studentpenalty', 'paydate');
    }
}