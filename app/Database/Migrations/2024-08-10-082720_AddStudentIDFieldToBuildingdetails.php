<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddStudentIDFieldToBuildingdetails extends Migration
{
    public function up()
    {
        $this->forge->addColumn('buildingdetails', [
            'studentid' => [
                'type'       => 'INT',
                'constraint' => '10',
            ],
           
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('buildingdetails', 'studentid');
    }
}