<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddNationalIdToStudents extends Migration
{
    public function up()
    {
        $this->forge->addColumn('students', [
            'NationalID' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('students', 'NationalID');
       
    }
}