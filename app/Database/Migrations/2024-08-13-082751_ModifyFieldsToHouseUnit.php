<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ModifyFieldsToHouseUnit extends Migration
{
    public function up()
    {
        $this->forge->modifyColumn('house_unit', [
            'supervisor' => [
                'type'       => 'varchar',
                'constraint' => '200',
                'default' => null,
            ],
        ]);
    }

    public function down()
    {
        $this->forge->modifyColumn('house_unit', [
            'supervisor' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => null,
            ],
        ]);
    }
}