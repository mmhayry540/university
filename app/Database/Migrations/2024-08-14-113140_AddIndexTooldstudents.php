<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddIndexTooldstudents extends Migration
{
    public function up()
    {
        $this->forge->addColumn('oldstudents0', [
            'INDEX index_fullname (`fullname`)'
        ]);
        $this->forge->addColumn('oldstudents1', [
            'INDEX index_fullname (`fullname`)'
        ]);
        $this->forge->addColumn('oldstudents2', [
            'INDEX index_fullname (`fullname`)'
        ]);
        $this->forge->addColumn('oldstudents3', [
            'INDEX index_fullname (`fullname`)'
        ]);
        $this->forge->addColumn('oldstudents4', [
            'INDEX index_fullname (`fullname`)'
        ]);
        $this->forge->addColumn('oldstudents5', [
            'INDEX index_fullname (`fullname`)'
        ]);
        $this->forge->addColumn('oldstudents6', [
            'INDEX index_fullname (`fullname`)'
        ]);
        $this->forge->addColumn('oldstudents7', [
            'INDEX index_fullname (`fullname`)'
        ]);
        $this->forge->addColumn('oldstudents8', [
            'INDEX index_fullname (`fullname`)'
        ]);
        $this->forge->addColumn('oldstudents9', [
            'INDEX index_fullname (`fullname`)'
        ]);
        $this->forge->addColumn('oldstudents10', [
            'INDEX index_fullname (`fullname`)'
        ]);
    }

    public function down()
    {
        $this->forge->dropIndex('oldstudents0', 'index_fullname');
        $this->forge->dropIndex('oldstudents1', 'index_fullname');
        $this->forge->dropIndex('oldstudents2', 'index_fullname');
        $this->forge->dropIndex('oldstudents4', 'index_fullname');
        $this->forge->dropIndex('oldstudents5', 'index_fullname');
        $this->forge->dropIndex('oldstudents6', 'index_fullname');
        $this->forge->dropIndex('oldstudents7', 'index_fullname');
        $this->forge->dropIndex('oldstudents8', 'index_fullname');
        $this->forge->dropIndex('oldstudents9', 'index_fullname');
        $this->forge->dropIndex('oldstudents10', 'index_fullname');
    }
}