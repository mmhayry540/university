<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddReceipts extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'firstroom' => [
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'lastroom' => [
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'housebuilding_id' => [
                'type'       => 'INT',
                'constraint' => '5',
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('receipts');
    }

    public function down()
    {
        $this->forge->dropTable('receipts');
    }
}