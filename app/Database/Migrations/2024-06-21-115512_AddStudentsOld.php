<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddStudentsOld extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'fullname' => [
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'mother' => [
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'mobile' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null' => true,
            ],
            'study_year' => [
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'study_part' => [ // الفصل الدراسي 1: فصل أول - 2 : فصل ثاني  3: صيفي
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'housebuilding_id' => [
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'roomno' => [
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'status' => [  // Innocent = 1 بريء ; Penalty = 2 يوجد غرامة , Progress = 3 قيد الدفع
                'type'       => 'INT',
                'constraint' => '5',
            ],
           
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('studentsold');
    }

    public function down()
    {
        $this->forge->dropTable('studentsold');
    }
}