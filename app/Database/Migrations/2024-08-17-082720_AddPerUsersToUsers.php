<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddPerUsersToUsers extends Migration
{
    public function up()
    {
        $this->forge->addColumn('users', [
            'per_users' => [
                'type'       => 'tinyint',
                'constraint' => '1',
                'default' => 0,
            ],
           
        ]);
        $this->forge->modifyColumn('users', [
            'per_addpenalty' => [
                'default' => 0,
            ],
            'per_cutpayment' => [
                'default' => 0,
            ],
            'per_confpayment' => [
                'default' => 0,
            ],
        ]);
      
    }

    public function down()
    {
        $this->forge->dropColumn('users', 'per_users');
        
    }
}