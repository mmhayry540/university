<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ModifyDefaultValueOfPenaltySingle extends Migration
{
    public function up()
    {

        $this->forge->modifyColumn('studentpenalty', [
            'status' => [
                'type'          => 'tinyint',
                'default'       => 0,
                'null'          => false,
            ],
        ]);
        
    }

    public function down()
    {
        $this->forge->modifyColumn('studentpenalty', [
            'status' => [
                'type'          => 'tinyint',
                 'null'          => true,
            ],
        ]);
    }
}