<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddClassFieldToBuildingdetails extends Migration
{
    public function up()
    {
        $this->forge->addColumn('buildingdetails', [
            'class' => [
                'type'       => 'VARCHAR',
                'constraint' => '10',
                'null' => true,
            ],
           
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('buildingdetails', 'class');
    }
}