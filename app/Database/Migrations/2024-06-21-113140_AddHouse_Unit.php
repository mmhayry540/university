<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddHouse_Unit extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
           
            'type' => [
                'type'       => 'tinyint',
                'constraint' => '1',
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('house_unit');
    }

    public function down()
    {
        $this->forge->dropTable('house_unit');
    }
}