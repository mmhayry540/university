<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ChangeFieldTypesInStudentsOld extends Migration
{
    public function up()
    {
        $this->forge->modifyColumn('studentsold', [
            'mobile' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
                'null'       => true,
            ],
        ]);
    }

    public function down()
    {
        
    }
}