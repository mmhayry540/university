<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddTypeFieldToStudentPenalty extends Migration
{
    public function up()
    {
        $this->forge->addColumn('studentpenalty', [
            'ptype' => [
                'type'       => 'tinyint',
                'constraint' => '1',
            ],
        ]);
    }

    public function down()
    {
        $this->forge->dropColumn('studentpenalty', 'ptype');
    }
}