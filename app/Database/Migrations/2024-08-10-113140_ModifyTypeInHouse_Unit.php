<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ModifyTypeInHouse_Unit extends Migration
{
    public function up()
    {

        $this->forge->modifyColumn('house_unit', [
            'type' => [
                'type'       => 'VARCHAR',
                'constraint' => 1,
            ],
        ]);
        
    }

    public function down()
    {
        $this->forge->modifyColumn('house_unit', [
            'type' => [
                'type'       => 'tinyint',
                'constraint' => 1,
            ],
        ]);
    }
}