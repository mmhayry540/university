<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddBuildingDetails extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'study_year' => [ // العام الدراسي
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'study_part' => [ // الفصل الدراسي 1: فصل أول - 2 : فصل ثاني  3: صيفي
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'housebuilding_id' => [ // الوحدة السكنية
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'roomno' => [ // رقم الغرفة
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'import_date' => [ //تاريخ الاستيراد
                'type'       => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ], 
           
        ]);
        $this->forge->addKey('id', true);
        $this->forge->createTable('buildingdetails');
    }

    public function down()
    {
        $this->forge->dropTable('buildingdetails');
    }
}