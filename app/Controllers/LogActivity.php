<?php

namespace App\Controllers;

use App\Models\student_Model;
use App\Models\User_Model;
use App\Models\UserActivityLog_Model;


class LogActivity extends BaseController
{
    public function __construct()
    {
        $this->new_student = new student_Model();
        $this->usermodel = new User_Model();
        $this->useractivitylog = new UserActivityLog_Model();
        $this->session = session();
    }

    public function index()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
    }

    public function log_system()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }

        if($this->request->getMethod() == 'post')
        {
            $startDate = date('Y-m-d H:i:s',strtotime($this->request->getPost('search_start_date')));
            $endDate= date('Y-m-d H:i:s',strtotime($this->request->getPost('search_end_date')));
            $user = $this->request->getPost('search_user');
            $data['userlogs'] = $this->useractivitylog->getlogdetail($startDate,$endDate,$user);
        }
        else{
            $data['userlogs'] = $this->useractivitylog->findAll();
        }
        //echo $data['userlogs'];
        $data['users'] = $this->usermodel->findAll();
        $data['title'] = "استعراض LOG";
        return view('logtrack/log_system', $data);
    }
}
