<?php

namespace App\Controllers;

use CodeIgniter\HTTP\ResponseInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use App\Models\OldStudent0_Model;
use App\Models\OldStudent1_Model;
use App\Models\OldStudent2_Model;
use App\Models\OldStudent3_Model;
use App\Models\OldStudent4_Model;
use App\Models\OldStudent5_Model;
use App\Models\OldStudent6_Model;
use App\Models\OldStudent7_Model;
use App\Models\OldStudent8_Model;
use App\Models\OldStudent9_Model;
use App\Models\OldStudent10_Model;

class ImportExcel extends BaseController
{

    public function __construct()
    {
        $this->oldstudents0 = new OldStudent0_Model();
        $this->oldstudents1 = new OldStudent1_Model();
        $this->oldstudents2 = new OldStudent2_Model();
        $this->oldstudents3 = new OldStudent3_Model();
        $this->oldstudents4 = new OldStudent4_Model();
        $this->oldstudents5 = new OldStudent5_Model();
        $this->oldstudents6 = new OldStudent6_Model();
        $this->oldstudents7 = new OldStudent7_Model();
        $this->oldstudents8 = new OldStudent8_Model();
        $this->oldstudents9 = new OldStudent9_Model();
        $this->oldstudents10= new OldStudent10_Model();
        $this->session = session();
    }
    public function index()
    {
        $data['title'] = "Import Excel";
        return view('importxls/import_excel' ,$data);
    }


    public function upload()
    {
        $file = $this->request->getFile('excel_file');

        if (!$file->isValid()) {
            return redirect()->back()->with('error', 'No file uploaded.');
        }

        $spreadsheet = new Spreadsheet();
        $reader = new Xlsx();
        $spreadsheet = $reader->load($file->getTempName());
        $worksheet = $spreadsheet->getActiveSheet();

        // Process the data from the worksheet
        $data = [];
        foreach ($worksheet->getRowIterator(1) as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            $rowData = [];
            foreach ($cellIterator as $cell) {
                $rowData[] = $cell->getValue();
                /*$name[] = $cell->getValue().'<br>';
                echo $name[0].'<br>'.$name[1];*/
                
            }
            $data[] = $rowData;
            
          
        }
        foreach($data as $da){
            if ($da[0] != ''){
                    //Temp1
                    $inserted['NationalID'] = '';
                    $inserted['study_year'] =  '';
                    $inserted['study_season'] = '';
                    $inserted['fullname'] = $da[0];
                    $inserted['father'] = '';
                    $inserted['mother'] = '';
                    $inserted['faculty'] = $da[3];
                    $inserted['student_id'] ='';
                    $inserted['partname'] ='';
                    $inserted['year'] ='';
                    $inserted['city'] = '';
                    $inserted['birthdate_place'] ='';
                    $inserted['kaid'] = '';
                    $inserted['address'] = '';
                    $inserted['building_id'] = $da[1];
                    $inserted['room_id'] = $da[2];
                    $inserted['penalty_detail'] = $da[4];
                    $inserted['penalty_date'] = '';
                    $inserted['penalty_value'] = '';
                    $inserted['order_id'] = '';
                    $inserted['order_date'] = '';
                    $inserted['order_notice1'] = $da[5];
                   
                    
                    $this->oldstudents10->insert($inserted);
            }
            
        }
        
        // Save the data to the database or perform other actions
        // ...

        //return redirect()->back()->with('success', 'Excel file imported successfully.');
    }
}