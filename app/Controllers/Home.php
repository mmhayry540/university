<?php

namespace App\Controllers;

use App\Models\student_Model;
use App\Models\student_penalty_Model;
use App\Models\User_Model;


class Home extends BaseController
{
    public function __construct()
    {
        $this->new_student = new student_Model();
        $this->usermodel = new User_Model();
        $this->student_penalty = new student_penalty_Model();
        $this->session = session();
    }

    public function index()
    {
        return redirect()->to(base_url('login'));
    }

    //====================================================
    //========= Test Result ==============================
    public function testresult(){
       echo  GetNameUnitById(2);
    }

    //====================================================

   
    // Login , Logout
    public function login()
    {
        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {
            $rules = [
                'username' => 'required',
                'password' => 'required'
            ];

            if (! $this->validate($rules)) {
                $data['validation'] = $this->validator;
            } else {

                $user = $this->usermodel->getUser($this->request->getVar('username'));

                if ($user && password_verify($this->request->getVar('password'), $user['password'])) {
                    $sessionData = [
                        'id'       => $user['id'],
                        'username' => $user['username'],
                        'isLoggedIn' => true
                    ];

                    session()->set($sessionData);
                    log_user_activity('قام المستخدم بتسجيل الدخول');
                    return redirect()->to(base_url('dashboard'));
                } else {
                    $data['validation'] = service('validation')->setError('username', 'خطأ اسم المستخدم أو كلمة السر');
                }
            }
        }

        $data['title'] = "تسجيل الدخول";
        return view('auth/login', $data);
    }
    //=================================

    public function logout()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        log_user_activity('قام المستخدم بعملية تسجيل الخروج');
        session()->destroy();
        return redirect()->to('/login');
    }

    //=================================
    public function pass()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        echo password_hash("admin", PASSWORD_DEFAULT);
    }
    //=================================

    // Dashboard
    public function dashboard()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }

        $data['students'] = $this->new_student->getNewStudentsList();

        $data['title'] = "لوحة التحكم";
        return view('dashboard', $data);
    }

    public function NewStudentsList()
    {
        $text = $this->request->getVar('s_text');
        $status = $this->request->getVar('s_status');
        $data['students'] = $this->new_student->getNewStudentsList($text , $status);
        return $this->response->setJSON($data);
    }

    public function OldStudentsList()
    {
        $text = $this->request->getVar('s_text');
        $data['students'] = $this->new_student->getoldstudent($text);
        return $this->response->setJSON($data);
    }

    public function get_student_data()
    {
        $id = $this->request->getVar('id');
        $data['student'] = $this->new_student->getStudentsData($id);
        $data['penalty'] = $this->new_student->GetPenaltyByStudentId($id);
        return $this->response->setJSON($data);
    }

    public function cut_penalty($penID, $stuID , $st, $ptype)
    {
        $data['pen'] = $penID;
        $data['stu'] = $stuID;
        $data['status'] = 2;
        $ptype= $ptype;
        $q = $this->new_student->ChangeStatusByPenaltyId($penID , $stuID , $data['status'],$ptype);
        log_user_activity('عملية قطع إيصال');
        $data['student'] = $this->new_student->getstudents_detail($stuID);
        if($ptype == 0) { $data['penalt'] = $this->new_student->getpenaltysingle_bystudentid_detail($stuID);}
        elseif($ptype == 1) { $data['penalt'] = $this->new_student->getpenaltycollectives_bystudentid_detail($stuID);}
       
       

        if ($q) {
            $data['title'] = "قطع إيصال الدفع";
            return view('print/cut_penalty', $data);
            print_r($data);
        } else {
            return redirect()->to(base_url('/dashboard'));
        }
    }

    public function confirm_penalty()
    {
        
            $penalty = $this->request->getVar('pen_id');
            $student= $this->request->getVar('st_id');
            $status= $this->request->getVar('status');
            $ptype= $this->request->getVar('ptype');
            $query = $this->new_student->ChangeStatusByPenaltyId($penalty , $student , $status,$ptype);
            log_user_activity('قام المستخدم بتسديد غرامة الطالب');
            if($query)
            {
                $data['status'] = true;
                $data['massage'] = 'تم تسديد الغرامة بنجاح !!';
            }else{
                $data['status'] = false;
                $data['massage'] = 'فشلت عملية تسديد الغرامة بسبب أخطاء في قاعدة البيانات ؟؟؟';
                
            }
            return $this->response->setJSON($data);
    }


    public function confirm_penalty_old()
    {
        
            $student= $this->request->getVar('st_id');
            $query = true ;//$this->new_student->ChangeStatusByPenaltyId($penalty , $student , $status);
            if($query)
            {
                $data['status'] = true;
                $data['massage'] = 'تم تسديد الغرامة بنجاح !!';
            }else{
                $data['status'] = false;
                $data['massage'] = 'فشلت عملية تسديد الغرامة بسبب أخطاء في قاعدة البيانات ؟؟؟';
                
            }
            return $this->response->setJSON($data);
    }
    public function search()
    {
    $text =  'سارة'; //$this->request->getVar('text'); 
    $status =-1; //$this->request->getVar('status'); 
    $data['students'] = $this->new_student->getstudent_detail($text , $status);
    return $this->response->setJSON($data);
    }

    
}
