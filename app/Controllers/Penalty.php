<?php

namespace App\Controllers;

use App\Models\Penalty_Single_Model;
use App\Models\Penalty_collective_Model;
use App\Models\Student_Model;
use App\Models\Student_penalty_Model;
use App\Models\Housing_Unit_model;
use App\Models\Suits_Model;

class Penalty extends BaseController
{
    public function __construct()
    {
        $this->p_single = new Penalty_Single_Model();
        $this->p_collective = new penalty_collective_Model();
        $this->student = new Student_Model();
        $this->student_penalty = new student_penalty_Model();
        $this->unit = new Housing_Unit_model();
        $this->suite = new Suits_Model();
        $this->session = session();
    }

    // this function to get penalty single / groups
    public function penalty_list()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }

        $type = $this->request->getVar('type');
        $search = $this->request->getVar('search');
        if ($type == "single") {
            $data = $this->p_single->getSinglePenaltyList($search);
        } else if ($type == "collective") {

            $data = $this->p_collective->getCollectivePenaltyList($search);
        } else {
            return "Error Request";
        }
        return $this->response->setJSON($data);
    }

    public function penalty_single()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }

        if ($this->request->isAJAX() && $this->request->getMethod() == 'post') {

            $penalty_id = $this->request->getPost('penalty_id');
            $info['housebuilding_id'] = $this->request->getPost('unit_number');
            $info['roomno'] = $this->request->getPost('room_number');
            $info['value'] = $this->request->getPost('penalty_value');
            $info['supervisor_orderdate'] = $this->request->getPost('supervisor_book_date');
            $info['supervisor_orderid'] = $this->request->getPost('supervisor_book_number');
            $info['orderid'] = $this->request->getPost('order_id');
            $info['orderdate'] = $this->request->getPost('order_date');
            $info['cause'] = $this->request->getPost('penalty_reason');
            $info['details'] = $this->request->getPost('panalty_details');
            $studentData = json_decode($this->request->getPost('students'), true);

            // add penalty to db
            if ($penalty_id === '') {

                if ($this->p_single->save($info)) {
                    $p_id = $this->p_single->getInsertID();
                    // add to student has penalty to db
                    if ($p_id) {

                        foreach ($studentData as $st) {
                            $student['stdid'] = $st['id'];
                            $student['penaltyid'] = $p_id; // Assuming $p_id is defined and contains the penalty ID
                            $student['status'] = 1;
                            $student['ptype'] = 0; // إفرادي
                            $this->student_penalty->save($student);
                        }
                    }
                    $data['status'] = 'success';
                    $data['massage'] = 'تم إضافة غرامة جديدة بنجاح !';
                    log_user_activity($data['massage']);
                } else {
                    $data['status'] = 'false';
                    $data['massage'] = 'تعذر إضافة غرامة جديدة بسبب أخطاء في قاعدة البيانات !';
                }

                return $this->response->setJSON($data);
            } else {
                // edit penalty 
                $this->student_penalty->where('penaltyid' , $penalty_id)->delete();

                foreach ($studentData as $st) {
                    $student['stdid'] = $st['id'];
                    $student['penaltyid'] = $penalty_id; // Assuming $p_id is defined and contains the penalty ID
                    $student['status'] = 1;
                    $student['ptype'] = 0; // إفرادي
                    $this->student_penalty->save($student);
                }
                if($this->p_single->update($penalty_id , $info))
                {
                    $data['status'] = 'success';
                    $data['massage'] = 'تم إضافة غرامة جديدة بنجاح !';
                    log_user_activity('قام المستخدم بتحديث بيانات الغرامة');
                    
                }else{
                    $data['status'] = 'false';
                    $data['massage'] = 'تعذر إضافة غرامة جديدة بسبب أخطاء في قاعدة البيانات !';
                }
                return $this->response->setJSON($data);
            }
        }

        $data = $this->p_single->getSinglePenaltyList();
        $data['units'] = $this->unit->findAll();
        $data['title'] = "التغريم الإفرادي";
        return view('penalty/penalty_single', $data);
    }

    public function penalty_collective()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        if ($this->request->isAJAX() && $this->request->getMethod() == 'post') {

            $penalty_id = $this->request->getPost('penalty_id');
            $info['housebuilding_id'] = $this->request->getPost('unit_number');
            $info['suiteno'] = $this->request->getPost('suit_number');
            $info['value'] = $this->request->getPost('penalty_value');
            $info['supervisor_orderdate'] = $this->request->getPost('supervisor_book_date');
            $info['supervisor_orderid'] = $this->request->getPost('supervisor_book_number');
            $info['orderid'] = $this->request->getPost('order_id');
            $info['orderdate'] = $this->request->getPost('order_date');
            $info['cause'] = $this->request->getPost('penalty_reason');
            $info['details'] = $this->request->getPost('panalty_details');
            $student['status'] = 1;
            $student['ptype'] = 1; // جماعي
            // add penalty to db
            if ($penalty_id === '') {

                if ($this->p_collective->save($info)) {
                    $pc_id = $this->p_collective->getInsertID();
                    $data['status'] = 'success';
                    $data['massage'] = 'تم إضافة غرامة جديدة بنجاح !';
                    log_user_activity($data['massage']);
                    //******************************* */
                    $studentData = $this->student->getstudentid_housebuilding_suitno($info['housebuilding_id'], $info['suiteno']);
                    foreach ($studentData as $st) {
                        $student['stdid'] = $st['studentid'];
                        $student['penaltyid'] = $pc_id; // Assuming $p_id is defined and contains the penalty ID
                        $student['status'] = 1;
                        $student['ptype'] = 1; // إفرادي
                        $this->student_penalty->save($student);
                    }

                } else {
                    $data['status'] = 'false';
                    $data['massage'] = 'تعذر إضافة غرامة جديدة بسبب أخطاء في قاعدة البيانات !';
                }

                return $this->response->setJSON($data);
            } else {
                // edit penalty
                 // delete penalty from studentpenalty
                if($this->p_collective->deletestudent_fromcollectivepenalty($penalty_id)){
                    if($this->p_collective->update($penalty_id , $info))
                        {
                            log_user_activity('تم تعديل بيانات التغريم الجماعي');
                            $studentData = $this->student->getstudentid_housebuilding_suitno($info['housebuilding_id'], $info['suiteno']);
                            foreach ($studentData as $st) {
                                $student['stdid'] = $st['studentid'];
                                $student['penaltyid'] = $penalty_id; // Assuming $p_id is defined and contains the penalty ID
                                $student['status'] = 1;
                                $student['ptype'] = 1; // إفرادي
                                $this->student_penalty->save($student);
                            }
                            $data['status'] = 'success';
                            $data['massage'] = 'تم إضافة غرامة جديدة بنجاح !';
                        }else{
                            $data['status'] = 'false';
                            $data['massage'] = 'تعذر إضافة غرامة جديدة بسبب أخطاء في قاعدة البيانات !';
                        }
                        return $this->response->setJSON($data);
                        }
                
            }
        }

        $data = $this->p_collective->getCollectivePenaltyList();
        $data['units'] = $this->unit->findAll();
        $data['suites'] = $this->suite->findAll();
        $data['title'] = "التغريم الجماعي";
        return view('penalty/penalty_collective', $data);
    }

    public function get_penalty_data_by_id()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        $type = $this->request->getVar('type');
        $id = $this->request->getVar('id');
        $unit_room = $this->p_single->get_unit_room_penalty($id);
        
        //echo $unit_room->housebuilding_id;
        if ($type === 'single') {
            $data['penalty'] = $this->p_single->find($id);
            $data['penalty']['students_penalty'] = $this->student->getpenaltysingle_student_detail($id);
            $data['penalty']['student_room'] = $this->student->getroomstudents($unit_room->roomno,$unit_room->housebuilding_id);
        } else {
            $data['penalty'] = $this->p_collective->find($id);
        }

        return $this->response->setJSON($data);
    }

    public function get_students_by_room_id()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        $room = $this->request->getVar('num');//101;
        $unit = $this->request->getVar('unit'); //2 ;
        $res = $this->student->getroomstudents($room,$unit);
        $data['student'] =  $res;//
        return $this->response->setJSON($data);
     
    }

    public function get_suites_by_unit_id()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        $unit = $this->request->getVar('unit'); //2 ;
        $data['suites'] = $this->suite->where('housebuilding_id' , $unit)->findAll();
        return $this->response->setJSON($data);

    }

    public function print_single_penalty($id)
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        log_user_activity('طباعة تغريم إفرادي');
        //****************************** */
        $data['students'] = $this->student->getpenaltysingle_student_detail($id);
        /******************************* */
        // to get penalty details
        $data['penalty'] = $this->student->getpenaltysingle_detail($id);

        $data['penalty'] =  $data['penalty'][0];
  
        // to get unit type
        $unit = $this->unit->select('type , supervisor')->where('id' , $data['penalty']['housebuilding_id'])->find(); 
        
        // to get student penalty
        $type = $unit[0];
        $data['type'] = $type['type'];
        $data['supervisor'] = $type['supervisor'];

        $data['title'] = "طباعة غرامة فردية";
        return view('/print/single_penalty_rep' , $data);
    }

    public function print_collective_penalty($id)
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }

        log_user_activity('طباعة تغريم جماعي');
        /******************************* */
        // to get penalty details
        $data['penalty'] = $this->p_collective->find($id);
  
        // to get unit type
        $unit = $this->unit->select('type, supervisor')->where('id' , $data['penalty']['housebuilding_id'])->find(); 
        
        // to get student penalty
        $type = $unit[0];
        $data['type'] = $type['type'];
        $data['supervisor'] = $type['supervisor'];

       
      
        $data['title'] = "طباعة غرامة جماعية";
        return view('/print/collective_penalty_rep' , $data);
    }

    public function add_payment_penalty()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to('/login');
        }
        $penaltyid = $this->request->getVar('penaltyid');
        $studentid = $this->request->getVar('studentid');
        $penaltyType = $this->request->getVar('penaltyType');
        $confirm_penalty_num = $this->request->getVar('confirm_penalty_num');
        $confirm_penalty_date = date('Y-m-d',strtotime($this->request->getVar('confirm_penalty_date')));
        //echo $this->student->ChangeStatusWhenPaid($penaltyid, $studentid, $penaltyType,$confirm_penalty_num, $confirm_penalty_date);
        //print_r($this->request->getVar());
        if($this->student->ChangeStatusWhenPaid($penaltyid, $studentid, $penaltyType,$confirm_penalty_num, $confirm_penalty_date)){
            echo 'true';
        }else{echo 'false';}/**/
        // return redirect()->to(base_url('/dashboard'));
    }
}
